/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "gc.h"
#include "ilist.h"
#include "str.h"
#include "digit.h"
#include "ecodes.h"
#include "ial.h"
#include "interpret.h"

#define TRUE 1
#define FALSE 0

tHtab * globalSymbTable;

tVal *  getSymbVal(tHtab *t, string *pKey){
	tHtabItem * item = htabFind(t, pKey);
	if(item == NULL){
		item = htabFind(globalSymbTable, pKey);
		if(item == NULL){
			printErr(ERR_RUN_OTHER);
		}
	}
	return &item->data->val;
}

tType *  getSymbType(tHtab *t, string *pKey){
	tHtabItem * item = htabFind(t, pKey);
	if(item == NULL){
		item = htabFind(globalSymbTable, pKey);
		if(item == NULL){
			printErr(ERR_RUN_OTHER);
		}
	}
	return &item->data->type;
}

tSymbol * getSymb(tHtab *t, string *pKey){
	tHtabItem * item = htabFind(t, pKey);
	if(item == NULL){
		item = htabFind(globalSymbTable, pKey);
		if(item == NULL){
			printErr(ERR_RUN_OTHER);
		}
	}
	return item->data;
}

int interpret(tList *L, tHtab *globalHtab)
{
	globalSymbTable = globalHtab;

	tInstr *Inst;
	listActFirst(L);

	tList activeTSlist;
	listInit(&activeTSlist);
	tList waitTSlist;
	listInit(&waitTSlist);
	tList deadTSlist;
	listInit(&deadTSlist);

	tList funcJumpList;
	listInit(&funcJumpList);

	listPush(&activeTSlist, globalHtab);
	tHtab *activeFrame = globalHtab;

	tHtab *waitFrame = NULL;
	tHtab *deadFrame = NULL;

	string interStr;
	strInit(&interStr);
	int readChar;

	while (L->Act != NULL) {
		Inst = listGetAct(L);
		PRD("interpretovaná instr (%d)\n",Inst->instType);
		switch (Inst->instType) {
			/* Arithmetic Instructions */
			case I_ADD:

				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->intval) = (getSymbVal(activeFrame,Inst->addr2)->intval) + (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->realval) = (getSymbVal(activeFrame,Inst->addr2)->realval) + (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(activeFrame,Inst->addr1)->stringval = strConcStr((getSymbVal(activeFrame,Inst->addr2)->stringval),(getSymbVal(activeFrame,Inst->addr3)->stringval));
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
					}

					listActNext(L);
					break;

			case I_SUB:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->intval) = (getSymbVal(activeFrame,Inst->addr2)->intval) - (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->realval) = (getSymbVal(activeFrame,Inst->addr2)->realval) - (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_MUL:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->intval) = (getSymbVal(activeFrame,Inst->addr2)->intval) * (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->realval) = (getSymbVal(activeFrame,Inst->addr2)->realval) * (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_DIV:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						if ((getSymbVal(activeFrame,Inst->addr3)->realval) != 0) {
							(getSymbVal(activeFrame,Inst->addr1)->realval) = (getSymbVal(activeFrame,Inst->addr2)->realval) / (getSymbVal(activeFrame,Inst->addr3)->realval);
						}
						else {
							printErr(ERR_RUN_ZERO_DIV);
						}
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_MOV:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						if(getSymb(activeFrame, Inst->addr2)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(activeFrame,Inst->addr1)->intval = getSymbVal(activeFrame,Inst->addr2)->intval;
						break;
					case T_REAL:
						if(getSymb(activeFrame, Inst->addr2)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(activeFrame,Inst->addr1)->realval = getSymbVal(activeFrame,Inst->addr2)->realval;
						break;
					case T_BOOL:
						if(getSymb(activeFrame, Inst->addr2)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(activeFrame,Inst->addr1)->boolval = getSymbVal(activeFrame,Inst->addr2)->boolval;
						break;
					case T_STRING:
						if(getSymb(activeFrame, Inst->addr2)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(activeFrame,Inst->addr1)->stringval = getSymbVal(activeFrame,Inst->addr2)->stringval;
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;



			/* Comparison */
			case I_ISEQUAL:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) == (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) == (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) == (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) == 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_ISNOTEQUAL:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) != (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) != (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) != (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) != 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_ISABOVE:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) > (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) > (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) > (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) > 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_ISBELOW:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) < (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) < (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) < (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) < 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_ISEQABOVE:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) >= (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) >= (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) >= (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) >= 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_ISEQBELOW:
				switch (*(getSymbType(activeFrame, Inst->addr2))) {
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->intval) <= (getSymbVal(activeFrame,Inst->addr3)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->realval) <= (getSymbVal(activeFrame,Inst->addr3)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval) <= (getSymbVal(activeFrame,Inst->addr3)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED) || !(getSymb(activeFrame, Inst->addr3)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(activeFrame,Inst->addr1)->boolval) = (strcmp(strGetStr(getSymbVal(activeFrame,Inst->addr2)->stringval),strGetStr(getSymbVal(activeFrame,Inst->addr3)->stringval)) <= 0);
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;



			/* Input/Output */
			case I_READ:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						strClear(&interStr);
						while (isspace((readChar = getchar())));
						strAddChar(&interStr,readChar);

						while ((readChar = getchar()) != EOF && readChar != '\n' && !isspace(readChar)) {
							strAddChar(&interStr,readChar);
						}
						PRD("INT READ AS: %s\n", strGetStr(&interStr));
						if (toInt(&interStr,&(getSymbVal(activeFrame,Inst->addr1)->intval))){
							printErr(ERR_RUN_NUMBER);
						}
						if(readChar != '\n' && readChar != EOF)
							while ( (readChar = getchar()) != '\n' && readChar != EOF);
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						break;
					case T_REAL:
						strClear(&interStr);
						while (isspace((readChar = getchar())));
						strAddChar(&interStr,readChar);

						while ((readChar = getchar()) != EOF && readChar != '\n' && !isspace(readChar)) {
							strAddChar(&interStr,readChar);
						}
						PRD("REAL READ AS: %s\n", strGetStr(&interStr));
						if ((toDouble(&interStr,&(getSymbVal(activeFrame,Inst->addr1)->realval))) == ERR_CONV) {
							printErr(ERR_RUN_NUMBER);
						}
						if(readChar != '\n' && readChar != EOF)
							while ( (readChar = getchar()) != '\n' && readChar != EOF);
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						break;
					case T_BOOL:
						printErr(ERR_SEM_TYPE);
						break;
					case T_STRING:
						strClear(&interStr);
						string * newString = malloc(sizeof(string));
						strInit(newString);
						while ((readChar = getchar()) != EOF && readChar != '\n') {
							strAddChar(&interStr,readChar);
						}

						strCopyString(newString, &interStr);
						getSymbVal(activeFrame,Inst->addr1)->stringval = newString;
						getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_WRITE:
				switch (*(getSymbType(activeFrame, Inst->addr1))) {
					case T_INT:
						if(getSymb(activeFrame, Inst->addr1)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						printf("%d", (getSymbVal(activeFrame,Inst->addr1)->intval));
						break;
					case T_REAL:
						if(getSymb(activeFrame, Inst->addr1)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						printf("%g", (getSymbVal(activeFrame,Inst->addr1)->realval));
						break;
					case T_BOOL:
						if(getSymb(activeFrame, Inst->addr1)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						if ((getSymbVal(activeFrame,Inst->addr1)->boolval) == TRUE) {
							printf("TRUE");
						}
						else {
							printf("FALSE");
						}
						break;
					case T_STRING:
						if(getSymb(activeFrame, Inst->addr1)->isDef != DEFINED){
							printErr(ERR_RUN_UNINI_VAR);
						}
						printf("%s", strGetStr(getSymbVal(activeFrame,Inst->addr1)->stringval));
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;



			/* Jumps */
			case I_JMP:
				listActFirst(L);
				tInstr * tempInstr;
				while(1){
					tempInstr = listGetAct(L);
					if((tempInstr->instType == I_LABEL) && (strCmpString(tempInstr->addr1, Inst->addr1) == 0)){
						break;
					}
					listActNext(L);
				}
				break;

			case I_JMPE:
				if(!getSymbVal(activeFrame, Inst->addr2)->boolval){
					listActFirst(L);
					tInstr * tempInstr;
					while(1){
						tempInstr = listGetAct(L);
						if((tempInstr->instType == I_LABEL) && (strCmpString(tempInstr->addr1, Inst->addr1) == 0)){
							break;
						}
						listActNext(L);
					}
				} else {
					listActNext(L);
				}
				break;

			case I_LABEL:
				listActNext(L);
				break;



			/* Conversion */
			case I_INT_TO_REAL:
				if(getSymb(activeFrame, Inst->addr2)->isDef != DEFINED){
					printErr(ERR_RUN_UNINI_VAR);
				}
				if(*(getSymbType(activeFrame, Inst->addr2)) != T_INT) {
					printErr(ERR_RUN_OTHER);
				}
				getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
				(getSymbVal(activeFrame,Inst->addr1)->realval) = (double)(getSymbVal(activeFrame,Inst->addr2)->intval);

				listActNext(L);
				break;



			/* Functions */
			case I_MAKE_FRAME:
				PP;
				PRD("MAKE FRAME from (%p)\n",Inst->addr1);
				waitFrame = htabClone(Inst->addr1);
				PZ;
				listPush(&waitTSlist, waitFrame);
				listActNext(L);
				break;

			case I_SET_PAR:
				switch (*(getSymbType(activeFrame, Inst->addr2))){
					case T_INT:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(waitFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(waitFrame,Inst->addr1)->intval) = (getSymbVal(activeFrame,Inst->addr2)->intval);
						break;
					case T_REAL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(waitFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(waitFrame,Inst->addr1)->realval) = (getSymbVal(activeFrame,Inst->addr2)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(waitFrame, Inst->addr1)->isDef = DEFINED;
						(getSymbVal(waitFrame,Inst->addr1)->boolval) = (getSymbVal(activeFrame,Inst->addr2)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(activeFrame, Inst->addr2)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(waitFrame, Inst->addr1)->isDef = DEFINED;
						getSymbVal(waitFrame,Inst->addr1)->stringval = getSymbVal(activeFrame,Inst->addr2)->stringval;
						break;
					default:
						printErr(ERR_RUN_OTHER);
						break;
				}

				listActNext(L);
				break;

			case I_CALL:
				waitFrame = listPop(&waitTSlist);
				listPush(&activeTSlist, waitFrame);
				activeFrame = waitFrame;
				waitFrame = listGetLast(&waitTSlist);

				listPush(&funcJumpList, Inst->addr2); // label
				listPush(&funcJumpList, Inst->addr3); // pKey for return

				if(strCmpConstStr(Inst->addr1, "sort") == 0){
					if((getSymb(activeFrame, strCreate("s"))->isDef != DEFINED)){
						printErr(ERR_RUN_UNINI_VAR);
					}
					getSymbVal(activeFrame,Inst->addr1)->stringval = strCreate("");
					getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
					sort(getSymbVal(activeFrame,strCreate("s"))->stringval, getSymbVal(activeFrame,Inst->addr1)->stringval);
				
				} else if(strCmpConstStr(Inst->addr1, "find") == 0){
					if((getSymb(activeFrame, strCreate("s"))->isDef != DEFINED) || !(getSymb(activeFrame, strCreate("search"))->isDef == DEFINED)){
						printErr(ERR_RUN_UNINI_VAR);
					}
					getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
					int position = find(getSymbVal(activeFrame, strCreate("s"))->stringval, getSymbVal(activeFrame, strCreate("search"))->stringval);
					getSymbVal(activeFrame, Inst->addr1)->intval = position;
				
				} else if(strCmpConstStr(Inst->addr1, "length") == 0){
					if((getSymb(activeFrame, strCreate("s"))->isDef != DEFINED)){
						printErr(ERR_RUN_UNINI_VAR);
					}
					getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
					int stringlength = length(getSymbVal(activeFrame,strCreate("s"))->stringval);
					getSymbVal(activeFrame, Inst->addr1)->intval = stringlength;
					PRD("INSTR fn length \n");

				} else if(strCmpConstStr(Inst->addr1, "copy") == 0){
					if((getSymb(activeFrame, strCreate("s"))->isDef != DEFINED) || !(getSymb(activeFrame, strCreate("i"))->isDef == DEFINED) || !(getSymb(activeFrame, strCreate("n"))->isDef == DEFINED)){
						printErr(ERR_RUN_UNINI_VAR);
					}

					getSymbVal(activeFrame,Inst->addr1)->stringval = strCreate("");
					getSymb(activeFrame, Inst->addr1)->isDef = DEFINED;
					copy(getSymbVal(activeFrame,strCreate("s"))->stringval, getSymbVal(activeFrame,Inst->addr1)->stringval, getSymbVal(activeFrame,strCreate("i"))->intval, getSymbVal(activeFrame,strCreate("n"))->intval);

				} else {
					listActFirst(L);
					tInstr * callTempInstr;
					while(1){
						callTempInstr = listGetAct(L);
						if((callTempInstr->instType == I_LABEL) && (strCmpString(callTempInstr->addr1, Inst->addr1) == 0)){
							break;
						}
						listActNext(L);
					}
					break;
				}

	
			case I_RETURN:
				deadFrame = listPop(&activeTSlist);
				listPush(&deadTSlist, deadFrame);
				listActLast(&activeTSlist);
				activeFrame = listGetAct(&activeTSlist);

				string * returnkey = listPop(&funcJumpList);
				string * labelkey = listPop(&funcJumpList);

				switch ( *(getSymbType(deadFrame, Inst->addr1)) ){
					case T_INT:
						if(!(getSymb(deadFrame,Inst->addr1)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, returnkey)->isDef = DEFINED;
						(getSymbVal(activeFrame, returnkey)->intval) = (getSymbVal(deadFrame,Inst->addr1)->intval);
						break;
					case T_REAL:
						if(!(getSymb(deadFrame,Inst->addr1)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, returnkey)->isDef = DEFINED;
						(getSymbVal(activeFrame, returnkey)->realval) = (getSymbVal(deadFrame,Inst->addr1)->realval);
						break;
					case T_BOOL:
						if(!(getSymb(deadFrame,Inst->addr1)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, returnkey)->isDef = DEFINED;
						(getSymbVal(activeFrame, returnkey)->boolval) = (getSymbVal(deadFrame,Inst->addr1)->boolval);
						break;
					case T_STRING:
						if(!(getSymb(deadFrame,Inst->addr1)->isDef == DEFINED)){
							printErr(ERR_RUN_UNINI_VAR);
						}
						getSymb(activeFrame, returnkey)->isDef = DEFINED;
						getSymbVal(activeFrame, returnkey)->stringval = getSymbVal(deadFrame,Inst->addr1)->stringval;

						break;
					default:
						PRD("INSTR RETURN UNKNOWN TYPE\n");
						printErr(ERR_RUN_OTHER);
						break;
				}

				deadFrame = listPop(&deadTSlist);
				htabFree(deadFrame);
				deadFrame = listGetLast(&deadTSlist);

				listActFirst(L);
				tInstr * returnTempInstr;
				while(1){
					returnTempInstr = listGetAct(L);
					if((returnTempInstr->instType == I_LABEL) && (strCmpString(returnTempInstr->addr1, labelkey) == 0)){
						break;
					}
					listActNext(L);
				}

				break;
		}
	}
	return OK;
}
