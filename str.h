/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

/* hlavičkový soubor pro práci s ADT stringem */

typedef struct{
	char* str;		/* ukazatel na string */
	int length;		/* délka stringu (počet znaku) */
	int allocSize; 	/* velikost alokované paměti  */
} string;

int strInit(string *s);
void strFree(string *s);

int strClear(string *s);
int strAddChar(string *s1, char c);
int strCopyString(string *s1, string *s2);
string * strClone(string *s);
string * strCreate(char *s);
int strCmpString(string *s1, string *s2);
int strCmpConstStr(string *s1, char *s2);
string * strConcStr(string *s1, string *s2);

char *strGetStr(string *s);
int strGetLength(string *s);

int strInsertStr(string *s1, char *c);
int toString(string *s1);
