var
  a: Integer;
  vysl: Integer;

function factorial(n : Integer): Integer;
var
  temp_result : Integer;
  decremented_n : Integer;
begin
  if n < 2 then 
    begin
      factorial := 1
    end
  else 
    begin
      decremented_n := n - 1;
      temp_result := factorial(decremented_n);
      factorial := n * temp_result
    end
end;



begin
  write('Zadejte cislo pro vypocet faktorialu: ');
  readln(a);
  if a < 0 then
    begin
        write('Faktorial nelze spocitat'#10'')
    end
  else
    begin
      vysl := factorial(a);
      write('Vysledek je: ', vysl, ''#10'')

    end
end.