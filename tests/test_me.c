/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */
#define t_assert(message, test) do { if (!(test)) return message; } while (0)
#define t_run_test(test) do { char *message = test(); if (message) return message; } while (0)

#include <stdio.h>
#include <string.h>

//nahodime moduly
#include "../ecodes.h"
#include "../str.h"
#include "../scanner.h"
#include "../ilist.h"
#include "../ial.h"
#include "../digit.h"
#include "../gc.h"
#include "../interpret.h"

static char * test_str() {
	string retezec;
	strInit(&retezec);

	for (char i = 'a'; i <= 'z'; i++){
		strAddChar(&retezec, i);
	}

	char *abeceda = "abcdefghijklmnopqrstuvwxyz";

 	t_assert("Chyba: str.h - v inicializovani retezce!", strCmpConstStr(&retezec, abeceda) == 0);

	string * x = strClone(&retezec);

 	t_assert("Chyba: str.h - kloni utoci", (x != &retezec && strCmpConstStr(x, abeceda) == 0));

 	strClear(&retezec);

	t_assert("Chyba: str.h - v mazani retezce!", strGetLength(&retezec) == 0);

	char *zadek = "Zadek";
	strInsertStr(&retezec, zadek);

	t_assert("Chyba: str.h - v porovnavani rezezce!", strCmpConstStr(&retezec, zadek) == 0);

	string retezec2;
	strInit(&retezec2);
	strCopyString(&retezec, &retezec2);

	t_assert("Chyba: str.h - v kopirovani rezezce!", strCmpString(&retezec, &retezec2) == 0);

	strClear(&retezec);
	strClear(&retezec2);

	char *zadek_maly = "A";
	strInsertStr(&retezec, zadek_maly);
	char *zadek_velky = "a";
	strInsertStr(&retezec2, zadek_velky);

	t_assert("Chyba: str.h - v porovnavani rezezce (insensitive)!", strCmpString(&retezec, &retezec2) == 0);

	strClear(&retezec);
	strAddChar(&retezec, '/');

	strCopyString(&retezec2, &retezec);

	t_assert("Chyba: str.h - v kopirovani rezezce (z mensiho do vetsiho)!", strCmpString(&retezec, &retezec2) == 0);

	strClear(&retezec);
	strClear(&retezec2);
	char *tstring = "'prdel''zadek'#060'asd'";
	char *truestring = "prdel'zadek<asd";
	strInsertStr(&retezec, tstring);
	strInsertStr(&retezec2, truestring);

	toString(&retezec);

	t_assert("Chyba: str.h - v prevodu stringu na stringu bez escape seq.!", strCmpString(&retezec, &retezec2) == 0);

	string stredniHovno;
	strInit(&stredniHovno);
	string dlouhyHovno;
	strInit(&dlouhyHovno);
	char *charovyHovno = "abcabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnopdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
	strInsertStr(&dlouhyHovno, charovyHovno);
	strCopyString(&stredniHovno, &dlouhyHovno);

	t_assert("Chyba: str.h - kopirovani jak hoven!", strCmpString(&dlouhyHovno, &stredniHovno) == 0);

	char *testcmpstr = "asdasdkjhakdjh";
	strInsertStr(&retezec, testcmpstr);
	strInsertStr(&retezec2, testcmpstr);

	t_assert("Chyba: str.h - v porovnani stejnych retezcu pomoci jejich pointeru!", strCmpString(&retezec, &retezec2) == 0);

	testcmpstr = "Zade";
	strInsertStr(&retezec, testcmpstr);
	testcmpstr = "Prdel";
	strInsertStr(&retezec2, testcmpstr);
	string *vysledek;
	vysledek = strConcStr(&retezec,&retezec2);

	t_assert("Chyba: str.h - v konkatenaci", strCmpConstStr(vysledek, "ZadePrdel") == 0);

	strFree(&retezec);
	strFree(&retezec2);
	strFree(vysledek);

 	return 0;
}

/* váže se na soubor tests/tokens.txt */

tToken returnedtokenIDs[] = {
	INT,
	ID,
	INT,
	ID,
	ID,
	ID,
	REAL,
	REAL,
	REAL,
	REAL,
	REAL,
	REAL,
	REAL,
	REAL,
	REAL,
	ID,
	INT,
	PLUS,
	ID,
	INT,
	MINUS,
	ID,
	STRING,
	STRING,
	LEX_ERR,
	STRING,
	STRING,
	LEX_ERR,
	STRING,
	MUL,
	STRING,
	LEX_ERR,
	STRING
};

char errMsg[100];

static char * test_scanner() {

	FILE *fp = fopen("tests/tokens.txt", "r");

	setSourceFile(fp);

	string attr;
	strInit(&attr);
	tToken token;

	for(int i = 0; (token = getNextToken(&attr)) != END_OF_FILE; i++){
		sprintf(errMsg,"Chyba: scanner.h - token n. %d, attr(%s) token(%d) expected (%d)",i, strGetStr(&attr), token, returnedtokenIDs[i]);
		t_assert(errMsg, token == returnedtokenIDs[i]);
	}

	// uvolňování
	strFree(&attr);
	fclose(fp);

	return 0;
}

static char * test_ilist() {

	tList MyList;
	listInit(&MyList);

	char* testchar0 = "zadek0";
	char* testchar1 = "zadek1";
	char* testchar2 = "zadek2";
	char* testchar3 = "zadek3";
	char* testchar4 = "zadek4";
	char* testchar5 = "zadek5";
	char* testchar6 = "zadek6";
	char* testchar7 = "zadek7";
	char* testchar8 = "zadek8";
	char* testchar9 = "klubicek";

	listPush(&MyList, testchar0);
	listPush(&MyList, testchar1);
	listPush(&MyList, testchar2);
	listPush(&MyList, testchar3);
	listPush(&MyList, testchar4);
	listPush(&MyList, testchar5);
	listPush(&MyList, testchar6);
	listPush(&MyList, testchar7);
	listPush(&MyList, testchar8);

	listActFirst(&MyList);

	t_assert("Chyba: ilist.c - listActivity fail -> listPush or listActFirst or listInit", listActive(&MyList) == 1);

	for(int i = 0; i<3; i++){
		listActNext(&MyList);
	}

	char* test = listGetAct(&MyList);

	t_assert("Chyba: ilist.c - listGetAct ot list listActNext", strcmp(test, testchar3) == 0);

	test = listPopAct(&MyList);

	t_assert("Chyba: ilist.c - listPopAct", strcmp(test, testchar3) == 0);

	test = listPop(&MyList);

	t_assert("Chyba: ilist.c - listPop", strcmp(test, testchar8) == 0);

	test = listPopFirst(&MyList);

	t_assert("Chyba: ilist.c - listPopFirst", strcmp(test, testchar0) == 0);

	listActGoto(&MyList, testchar6);
	test = listPopAct(&MyList);

	t_assert("Chyba: ilist.c - listActGoto", strcmp(test, testchar6) == 0);

	listActLast(&MyList);
	listPushBeforeAct(&MyList, testchar9);
	listActPrev(&MyList);
	test = listGetAct(&MyList);

	t_assert("Chyba: ilist.c - listPushBeforeAct", strcmp(test, testchar9) == 0);

	listActFirst(&MyList);
	listPushAfterAct(&MyList, testchar9);
	listActNext(&MyList);
	test = listGetAct(&MyList);

	t_assert("Chyba: ilist.c - listPushAfterAct", strcmp(test, testchar9) == 0);

	listPushFirst(&MyList, testchar3);
	test = listGetFirst(&MyList);

	t_assert("Chyba: ilist.c - listPushFirst or listGetFirst", strcmp(test, testchar3) == 0);

	listFree(&MyList);

	t_assert("Chyba: ilist.c - listFree", MyList.First == NULL);

 	return 0;
}

static char * test_htable() {

	tHtab * table;
	int size  = 8;
	table = htabInit(size);

	t_assert("Chyba: ial.c - htabInit", table != NULL);

	tSymbol a, b, c;
	a.isDef = false;
	a.type = T_INT;

	b.isDef = true;
	b.type = T_REAL;

	c.isDef = true;
	c.type = T_BOOL;


	string ht_retezec1;
	strInit(&ht_retezec1);

	char *helpstring1 = "Zadek";
	strInsertStr(&ht_retezec1, helpstring1);

	string ht_retezec2;
	strInit(&ht_retezec2);

	char *helpstring2 = "zadek";
	strInsertStr(&ht_retezec2, helpstring2);

	string ht_retezec3;
	strInit(&ht_retezec3);

	char *helpstring3 = "Karel";
	strInsertStr(&ht_retezec3, helpstring3);

	int rslt = -1;

	rslt = htabInsert(table, &ht_retezec1, &a, NULL);
	t_assert("Chyba: ial.c - htabInsert 1", rslt == HT_OK);

	rslt = htabInsert(table, &ht_retezec2, &b, NULL);
	t_assert("Chyba: ial.c - htabInsert 2", rslt == HT_REDEF);

	rslt = htabInsert(table, &ht_retezec3, &c, NULL);
	t_assert("Chyba: ial.c - htabInsert 3", rslt == HT_OK);

	rslt = htabInsert(table, &ht_retezec3, &b, NULL);
	t_assert("Chyba: ial.c - htabInsert 4", rslt == HT_REDEF);

	tHtabItem * x;
	x = htabFind(table, &ht_retezec3);
	t_assert("Chyba: ial.c - htabFind 1", x != NULL && x->data->type == c.type);

	tHtabItem * y;
	y = htabFind(htabClone(table), &ht_retezec3);
	t_assert("Chyba: ial.c - htabClone()", y != NULL && y->data->type == c.type);

	x = htabFind(table, &ht_retezec2);
	t_assert("Chyba: ial.c - htabFind 2", x !=  NULL && x->data->type == a.type);

	// uvolňování
	htabFree(table);

	strFree(&ht_retezec1);
	strFree(&ht_retezec2);
	strFree(&ht_retezec3);
	return 0;
}

static char * test_sort(){

	string sort1;
	string sort2;
	strInit(&sort1);
	strInit(&sort2);
	char *sortchar = "dehkjsdhgkshkfhsdkfjhdskffffva";
	char *rightstr = "addddeffffffghhhhhjjkkkkkssssv";

	strInsertStr(&sort1, sortchar);
	sort(&sort1, &sort2);

	t_assert("Chyba: ial.c - mergeSort fail - pismena", strCmpConstStr(&sort2, rightstr) == 0);

	sortchar = "aslkjrrrhlkajsdn112876wes5";
	rightstr = "1125678aadehjjkkllnrrrsssw";
	strClear(&sort1);
	strInsertStr(&sort1, sortchar);
	sort(&sort1, &sort2);

	t_assert("Chyba: ial.c - mergeSort fail - pismena a cisla", strCmpConstStr(&sort2, rightstr) == 0);

	sortchar = "7756783224566789622141165421";
	rightstr = "1111222223444555666667777889";
	strClear(&sort1);
	strInsertStr(&sort1, sortchar);
	sort(&sort1, &sort2);

	t_assert("Chyba: ial.c - mergeSort fail - cisla", strCmpConstStr(&sort2, rightstr) == 0);

	sortchar = "asdlkjw---asd.))weaf";
	rightstr = "))---.aaaddefjklssww";
	strClear(&sort1);
	strInsertStr(&sort1, sortchar);
	sort(&sort1, &sort2);

	t_assert("Chyba: ial.c - mergeSort fail - jine znaky", strCmpConstStr(&sort2, rightstr) == 0);

	return 0;
}

static char * test_digit() {

	string dgt_str;
	strInit(&dgt_str);


	/* Testy toDouble */
	double x;

	strInsertStr(&dgt_str,"12345.3");
	toDouble(&dgt_str,&x);
	t_assert("Chyba digit.c - toDouble X.Y", x == (double) 12345.3);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"12345E3");
	toDouble(&dgt_str,&x);
	t_assert("Chyba digit.c - toDouble XeZ", x == (double) 12345e3);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"12345.32E222");
	toDouble(&dgt_str,&x);
	double eps=0.01*x; // Odchylka z duvodu nepresnosti mantisy..
	t_assert("Chyba digit.c - toDouble (X.Y)ez", (x <= (double) 1.234532e226 + eps && x >= (double) 1.234532e226 - eps));
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"12345.32E2222");
	t_assert("Chyba digit.c - toDouble IEEE 754", toDouble(&dgt_str,&x) == ERR_CONV);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"123553E-3");
	toDouble(&dgt_str,&x);
	t_assert("Chyba digit.c - toDouble Xe-Z", x == (double) 123553e-3);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"32.32EA");
	t_assert("Chyba digit.c - toDouble X.Ye'znak'", toDouble(&dgt_str,&x) == ERR_CONV);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"-3232.5E-0");
	toDouble(&dgt_str,&x);
	t_assert("Chyba digit.c - toDouble -X.Ye-Z", x == (double) -3232.5E-0);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"32.32E-A");
	t_assert("Chyba digit.c - toDouble X.Ye-'znak'", toDouble(&dgt_str,&x) == ERR_CONV);
	strClear(&dgt_str);


	strInsertStr(&dgt_str,"0");
	toDouble(&dgt_str,&x);
	t_assert("Chyba digit.c - toDouble 0", x == (double) 0);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"0");
	t_assert("Chyba digit.c - toDouble 0", toDouble(&dgt_str,&x) == OK);
	strClear(&dgt_str);

	/* Testy toInt */
	int y;


	strInsertStr(&dgt_str,"12345");
	toInt(&dgt_str,&y);
	t_assert("Chyba digit.c - toInt X", y == 12345);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"-12345");
	toInt(&dgt_str,&y);
	t_assert("Chyba digit.c - toInt -X", y == -12345);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"1234553121564115511516546");
	t_assert("Chyba digit.c - toInt velké X", toInt(&dgt_str,&y) == ERR_CONV);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"0");
	toInt(&dgt_str,&y);
	t_assert("Chyba digit.c - toInt 0", y == 0);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"1E");
	t_assert("Chyba digit.c - toInt 0", toInt(&dgt_str,&y) == ERR_CONV);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"E");
	t_assert("Chyba digit.c - toInt 0", toInt(&dgt_str,&y) == ERR_CONV);
	strClear(&dgt_str);

	strInsertStr(&dgt_str,"1");
	toInt(&dgt_str,&y);
	t_assert("Chyba digit.c - toInt 0", y == 1);
	t_assert("Chyba digit.c - toInt 0",	toInt(&dgt_str,&y) == OK);
	strClear(&dgt_str);

	strFree(&dgt_str); // Nice and Clean
	return 0;
}

static char * test_find() {

	string s;
	string search;
	strInit(&s); strInit(&search);


	strInsertStr(&s,"zadniceprdelpulkypozadizadekprdelkatentononcsedaci");
	strInsertStr(&search, "zadek");
	int i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice", i == 24);
	strClear(&s); strClear(&search);

	strInsertStr(&s,"zadniceprdelpulkypozadzadekprdelkatentononcsedaci");
	strInsertStr(&search, "zadek");
	i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice2", i == 23);
	strClear(&s); strClear(&search);

	strInsertStr(&s,"zadniceprdelpulkypozadkprdelkatentononcsedacizadek");
	strInsertStr(&search, "zadek");
	i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice3", i == 46);
	strClear(&s); strClear(&search);

	strInsertStr(&s,"zadniceprdelpulkypozadkprdelkatentononcsedacizadek");
	strInsertStr(&search, "zadnice");
	i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice3", i == 1);
	strClear(&s); strClear(&search);

	strInsertStr(&s,"zadniceprdelpulkypozadkprdelkatentononcsedacizadek");
	strInsertStr(&search, "");
	i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice3", i == 1);
	strClear(&s); strClear(&search);

	strInsertStr(&s,"zadniceprdelpulkypozadkprdelkatentononcsedacizadek");
	strInsertStr(&search, "dniceprd");
	i = find(&s,&search);
	t_assert("Chyba find.c - Spatna pozice3", i == 3);
	strClear(&s); strClear(&search);


	strFree(&s), strFree(&search);
	return 0;
}

static char * test_symClone() {

	tSymbol testSym;

	testSym.isDef = false;
	testSym.type = T_STRING;

	string stringos;
	strInit(&stringos);

	char * hovnochar = "hovno";
	strInsertStr(&stringos, hovnochar);

	testSym.val.stringval = &stringos;

	tSymbol * clonSym = symClone(&testSym);

	t_assert("Chyba ial.c - symClone()", &testSym != clonSym);

	return 0;
}


static char * all_tests() {
	t_run_test(test_str); //str.h
	t_run_test(test_scanner); //scanner.h
	t_run_test(test_ilist); //ilist.h
	t_run_test(test_htable); //ial.h
	t_run_test(test_sort); //ial.h
	t_run_test(test_digit); //digit.h
	t_run_test(test_find); //find.h
	t_run_test(test_symClone); //find.h

	return 0;
}

int main(/*int argc, char **argv*/) {
	gcInit();

	char *result = all_tests();

	if (result != 0) {
		printf("%s\n", result);
	} else {
		printf("Ty kravo, zadna chyba, huste!\n");
	}

	gcClear();

	return (result) ? 1 : 0;
}
