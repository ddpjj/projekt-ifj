/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

/* výčet hodnot vracených fcí getNextToken */
typedef enum tokens {
	// až po dolar nutné zachovat pořadí, slouží jako indexy do PrtableOP v parseru
	MUL, // *
	DIV, // /
	PLUS, // +
	MINUS, // -
	LESS , // <
	MORE, // >
	LESS_EQU, // <=
	MORE_EQU, // >=
	NON_EQU, // <>
	EQU, // =
	L_PARENTH = 10, // (
	R_PARENTH, // )
	
	ID, // identifikátor proměnné, či fce

	/* = konstanty = */
	INT,  // C unsigned int
	REAL, // C double
	STRING, // ''
	BOOL, // true, false

	DOLLAR,

	/* = klíčová  slova = */
	VAR = 20, // var
	BEGIN, // begin
	END, // end
	THEN, // then
	DO, // do
	IF, // if
	WHILE, // while
	ELSE, // else
	REPEAT, // repeat
	UNTIL, // until
	FUNCTION, // function
	FORWARD, // forward 

	READLN,
	WRITE,

	TYPE = 40, // integer, real, string, boolean

	COLON, // :
	SEMICOLON, // ;
	COMMA, // ,
	DOT, // .

	ASSIGN, // :=

	END_OF_FILE,

	LEX_ERR,
	
	// pro učely precedenční analýzy výrazů
	E = 50, 
	RED,
	STOP,
	ANY, // pro potřeby pokrytí libovolného tokenu


} tToken;

void setSourceFile(FILE*);
tToken getNextToken(string*);
