/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include "ecodes.h"
#include "str.h"
#include "scanner.h"
#include "ilist.h"
#include "ial.h"
#include "parser.h"
#include "interpret.h"
#include "digit.h"
#include "gc.h"

#define THE_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING 42
 
/* === MAIN === */

int main(int argc, char const *argv[]){
	if(gcInit() == GC_ERROR)
		return printErr(ERR_INTERNAL);

	if(argc != 2)
		return printErr(ERR_INTERNAL);

	FILE *fp = fopen(argv[1], "r");
	if(fp == NULL)
		return printErr(ERR_INTERNAL);

	setSourceFile(fp);

	tList instrStack;

	tHtab * GTS;

	listInit(&instrStack);

	int result = parser(&instrStack, &GTS);
	if(result)
		printErr(result);

	PRD("\nINTERPRETACE:\n");

	result = interpret(&instrStack, GTS);
	if(result)
		printErr(result);

	
	fclose(fp);

	gcClear();

	return result;
}

