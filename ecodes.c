/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "gc.h"
#include "ecodes.h"

/* errorové hlášky */
const char *ERR_MSGS[] = {
	"",
	"Chyba v programu v rámci lexikální analýzy (chybná struktura aktuálního lexému)",
	"Chyba v programu v rámci syntaktické analýzy (chyba syntaxe struktury programu",
	"Sémantický chyba v programu - nedefinovaná funkce/proměnná, pokus o redefinici funkce/proměnné, atd.",
	"Sémantická chyba typové kompatibility v aritmetických, řetězcových a relačních výrazech, příp. špatný počet či typ parametrů u volání funkce.",
	"Ostatní sémantické chyby.",
	"Běhový chyba při práci načítání číselné hodnoty ze vstupu",
	"běhová chyba při práci s neinicializovanou proměnnou",
	"Běhová chyba dělení nulou.",
	"Ostatní běhové chyby",
	"Interní chyba interpretu tj. neovlivněná vstupním programem (např. chyba alokace paměti, chyba při otevírání souboru s řídícím programem, špatné parametry příkazové řádky atd."
};

/* vytiskne error na stderr a vrací ecode */
int printErr(Ecode ecode){
	fprintf(stderr, "%s\n", ERR_MSGS[ecode]);
	gcClear();
	if(ecode == ERR_INTERNAL){
		ecode = 99;
	}

	exit(ecode);
}