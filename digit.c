/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include "str.h"
#include "digit.h"
#include "ecodes.h"


int toInt(string *xttr, int *result)
{
	char *attr=strGetStr(xttr);
	char *err;
	long convert;
	errno = 0; convert = strtol(attr,&err,10);
	if (errno == ERANGE || convert > INT_MAX || *err != '\0') {
		return ERR_CONV;
	}
	else {
		*result = convert;
		return OK;
	}
	return ERR_CONV;
}

double toDouble(string *xttr, double *result)
{
	char *attr=strGetStr(xttr);
	char *err;
	double convert;
	errno = 0; convert = strtod(attr, &err);
	if (errno == ERANGE || *err != '\0') { //isinf - IEEE 754
		return ERR_CONV;
	}
	else {
		*result = convert;
		return OK;
	}
	return ERR_CONV;
}
