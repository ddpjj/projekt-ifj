/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdbool.h>

/** hash table **/

/* symboly */

typedef enum {
	UNDEF,
	DECLARED,
	DEFINED,
} tDef;

typedef enum type {
	T_INT = 0,
	T_REAL,
	T_STRING,
	T_BOOL,
	T_FUNC,
	T_UNDEF,
} tType;

typedef union  {
	bool boolval;
	int intval;
	double realval;
	string * stringval;
	tList * head;
} tVal;

typedef struct Symbol {
	tDef isDef;
	tType type;
	tVal val;
} tSymbol;


/* položka tabulky */

typedef struct htabItem {
	string * key;
	tSymbol * data; // ukazatel na záznam (symbol)
	struct htabItem *next;
} tHtabItem;

/* hashovací tabulka */
typedef struct htab {
	int size;
	tHtabItem *data[]; // pole ukazatelů na položky (vázané seznamy)
} tHtab;

/* návratové hodnoty fcí */
enum htabEcodes {
	HT_OK,
	HT_ERROR,
	HT_REDEF,
};



/** deklarace fcí **/

/**
 * vytvoření a inicializaci tabulky 
 * 
 * @param  size velikost
 * @return NULL v případě chyby, jinak hashovací tabulku
 */
tHtab * htabInit(int size);

/**
 * vyhledávání
 * 
 * @param  t ukazatel na tabulku symbolů
 * @param  key string
 * @return	vrací item data, nebo NULL pokud nenalezeno
 */
tHtabItem * htabFind(tHtab *t, string *key);

/**
 * vloží prvek
 * 
 * @param  t hash table
 * @param  key string
 * @param  data záznam symbolu
 * @return HT_OK nebo HT_REDEF pokud už symbol existuje, HT_ERR v případě chyby
 */
int htabInsert(tHtab *t, string *key, void *data, tHtabItem ** newItem );

/**
 * zrušení celé tabulky 
 * 
 * @param t tabulka symbolů
 */
void htabFree(tHtab *t);

/**
 * Klonujem
 * @param  sym [description]
 * @return     [description]
 */
tSymbol * symClone(tSymbol * sym);

/**
 * Klonujem
 * @param  t [description]
 * @return   [description]
 */
tHtab * htabClone(tHtab *t);

unsigned int getHash(string *str, unsigned htabSize);

/* řazení v retezci pomoci merge sort */
void sort(string *s1, string *s2);

/* hledání pomocí Boyer-Moore algoritmu */
int find(string *s, string *search);

/* ziskání délky stringu */
int length(string *s1);

/* kopírování stringu z druhého stringu od startpos délky length */
void copy(string *s1, string *s2, int startpos, int length);
