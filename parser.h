/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

/**
 * hlavní fce programu, dělá vše
 * @return OK nebo ERR_SYNTAX nebo ERR_LEXEM (.. nebo jiný ERR)
 */
Ecode parser ();