/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include "gc.h"
#include "ecodes.h"
#include "str.h"
#include "digit.h"
#include "scanner.h"
#include "ilist.h"
#include "ial.h"
#include "parser.h"
#include "interpret.h"

#define switch(expr) switch((unsigned int)expr)

typedef enum ruleIndexes { // prvních 10 neměnit!
	R_PLUS,
	R_MINUS,
	R_MUL,
	R_DIV,
	R_EQU,
	R_NON_EQU,
	R_LESS,
	R_MORE,
	R_LESS_EQU,
	R_MORE_EQU,
	R_ID,
	R_INT,
	R_REAL,
	R_BOOL,
	R_STRING,
	R_PAR,
} tRuleIndex;

typedef enum prTableOP {
	_LESS, // <3
	_MORE, // >
	_EQU, // =
	_ERR, // err
	_FNC, // volání funkce fn()
} tPrTableOP;


/* precedenční tabulka */

static tPrTableOP prTable[DOLLAR+1][DOLLAR+1] = {
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // *
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // /
	{_LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // +
	{_LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // -
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // <
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // >
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // <=
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // >=
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // <>
	{_LESS, _LESS, _LESS, _LESS, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _LESS, _MORE, _LESS, _LESS, _LESS, _LESS, _LESS, _MORE}, // =
	{_LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _EQU,  _LESS, _LESS, _LESS, _LESS, _LESS, _ERR }, // (
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _ERR,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // )
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _FNC,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // ID
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _ERR,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // INT
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _ERR,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // REAL
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _ERR,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // STRING
	{_MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _MORE, _ERR,  _MORE, _ERR,  _ERR,  _ERR,  _ERR,  _ERR,  _MORE}, // BOOL
	{_LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _LESS, _ERR,  _LESS, _LESS, _LESS, _LESS, _LESS, _ERR }// $
};



/* konstanty */
const bool FAIL = 1;
const bool SUCCESS = 0;
const unsigned int HTAB_SIZE = 256;

/* funcke pravidel */
bool cover();
bool program();
bool funcsDef_body();
bool funcsDef();
bool varsDef();
bool varDefList();
bool varDefList();
bool varDefListEnd();
bool varDef();
bool funcDefEnd();
bool funcDef();
bool funcHead();
bool funcHeadParamsEnd();
bool funcHeadType();
bool paramsList();
bool paramsListEnd();
bool param();
bool funcHeadEnd_body();
bool funcBody();
bool funcsDef_body();
bool body();
bool commandBlock();
bool commandBlockEnd();
bool commandsList();
bool commandsListEnd();
bool command();
bool expr(bool);
bool repeatcommand();
bool whilecommand();
bool ifcommand();
bool assign();
bool fncall(tList*, tList*, string*);
bool writecall();
bool readlncall();
bool areAllFncsDef();
/*pomocné fce*/
string * getUniKey();
void insertInternFunc(char * fname, tType type, int len,  ...);
tHtabItem * createTempItem(tType type);
tType getResultType(tHtabItem * a, tHtabItem * b, tRuleIndex op);
tType getType(string * attr);
tInstr *  generateInstr(int type, void *pKey1, void *pKey2, void *pKey3);
int getInstrType(tRuleIndex rule);

/* === globalní proměnné === */

tToken token; // sem se bude ukládat každý další token
string attr; // a sem případný attribut (hodnota) tokenu

Ecode ecode = OK; // pro uložení návratové hodnoty parseru

tType actType = T_UNDEF; // typ výrazu ktery se pravě zpracovává 

tHtab * globalSTable; // globální tabulka symbolů
tHtab * actSTable; // aktuální tabulka symbolů

tHtabItem * actFunSym; // symbol v GTS aktuálně zpracovávané funkce

tHtabItem * lastItem; // uakazatel na navratovou hodnotu poslední expru

tList * globalInstrList; 

int paramCounter = 0;


string * dollarLabel; // název labelu začátku programu

/**
 * hlavní fce programu, dělá vše
 * @return OK nebo ERR_SYNTAX nebo ERR_LEXICAL (.. nebo jiný ERR)
 */
Ecode parser (tList * instrList, tHtab ** GTS){

	*GTS = globalSTable = htabInit(HTAB_SIZE);

	actSTable = globalSTable;

	PRD("ACT<-GTS: %p\n", (void*)actSTable);

	globalInstrList = instrList;

	dollarLabel = getUniKey();

	strInit(&attr);


	if(cover(ANY) == FAIL) // načtení prvního tokenu
		return ecode;

	bool result = FAIL;

	switch (token) { // první token musí být VAR, BEGIN nebo FUNCTION
		case VAR:
		case BEGIN:
		case FUNCTION:
			result = program();
			PRD("= ecode %d\n", ecode);
			if(result == FAIL && ecode == OK){ // pokud ecode neni specifikované, jde o syntax err
				ecode = ERR_SYNTAX;
			}
			break;
		default:
			ecode = ERR_SYNTAX;
	}

	return ecode;
}


/**
 * pomocná fce volaná při "pokrytí" tokenu	
 * porovná token s terminálem
 * získává další token
 */

bool cover(tToken terminal){

	if(terminal == token || terminal == ANY){
		//PRD("covered(%d)(%d)\n",token, actType);
		if((token = getNextToken(&attr)) == LEX_ERR){
			ecode = ERR_LEXICAL;
			return FAIL;
		}
		else 
			return SUCCESS;
	} else {
		PRD("> cover expected(%d) received(%d)\n", terminal, token);
		ecode = ERR_SYNTAX;
		return FAIL;
	}
}	






/************************************************************
 * Funkce pravidel, vždy frací SUCCESS nebo FAIL 
 */

bool program(){

	/* přidat interních fcá do GTS */

	generateInstr(I_JMP, dollarLabel, NULL, NULL); // skok na začátek programu


	insertInternFunc("length", T_INT, 1, "s", T_STRING);
	insertInternFunc("copy", T_STRING, 3, "s", T_STRING, "i", T_INT, "n", T_INT);
	insertInternFunc("find", T_INT, 2, "s", T_STRING, "search", T_STRING);
	insertInternFunc("sort", T_STRING, 1, "s", T_STRING);

	switch (token){
		case VAR: // pravidlo <program> -> <varsDef> <funcsDef_body> {VAR}
			return varsDef() || funcsDef_body() || areAllFncsDef();
		case FUNCTION: // pravidlo <program> -> <funcsDef> <body> {FUNCTION}
			return funcsDef() || body() ||  areAllFncsDef();
		case BEGIN: // pravidlo <program> -> <body> {BEGIN}
			return body() || areAllFncsDef();
	}

	return FAIL;
}




bool funcsDef_body(){
	switch (token){
		case FUNCTION:
			return funcsDef() || body(); // <funcsDef_body> -> <funcsDef> <body> {FUNCTION}
		case BEGIN:
			return body(); // <funcsDef_body> -> <body> {BEGIN}
	}
	return FAIL;
}

bool varsDef(){
	switch(token){
		case VAR: //	<varsDef> -> VAR <varDefList> {VAR} 
			return cover(VAR) || varDefList();
	}
	return FAIL;
}

bool varDefList(){
	switch(token){
		case ID: //		<varDefList> -> <varDef> <varDefListEnd> {ID} 
			return varDef() || varDefListEnd();
	}
	return FAIL;
}

bool varDefListEnd(){
	switch(token){
		case ID: // <varDefListEnd> -> <varDef> <varDefListEnd> {ID}
			return varDef() || varDefListEnd();
		default: // <varDefListEnd> -> eps {eps}
			return SUCCESS;
	}
}

bool varDef(){

	tSymbol * symbol;
	switch(token){
		case ID: // <varDef> -> ID : TYPE ; {ID}

			symbol = (tSymbol*) malloc(sizeof(tSymbol));
			symbol->isDef = UNDEF;

			if(htabInsert(actSTable, strClone(&attr), symbol, NULL) == HT_REDEF){
				ecode = ERR_SEM_DEF;
				return FAIL;
			}
			
			if(cover(ID) == FAIL)
				return FAIL;
			// token == :

			if(cover(COLON) == FAIL)
				return FAIL;
			// token == TYPE
			// 
			if(token != TYPE )
				return FAIL; 
			 
			symbol->type = getType(&attr);


			if(cover(TYPE) == FAIL)
				return FAIL;

			return cover(SEMICOLON);
	}

	return FAIL;
}

bool funcsDef(){
	switch(token){
		case FUNCTION:	//	<funcsDef> -> <funcDef> <funcDefEnd> {FUNCTION}
			return funcDef() || funcDefEnd();
	}
	return FAIL;
}

bool funcDefEnd(){
	switch(token){
		case FUNCTION: // <funcDefEnd> -> <funcDef> <funcDefEnd> {FUNCTION}
			return  funcDef() || funcDefEnd();
		default:	// <funcDefEnd> -> eps {eps}
			return SUCCESS;
	}
}

bool funcDef(){

	switch(token){
		case FUNCTION: // <funcDef> -> <funcHead> <funcHeadEnd_body> {FUNCTION}
			return funcHead() || funcHeadEnd_body() || (actSTable = globalSTable, SUCCESS);
	}
	return FAIL;
}

bool funcHead(){
	PRD("> funcHead\n");
	switch(token){
		case FUNCTION: //	<funcHead> -> FUNCTION ID ( <funcHeadParamsEnd> {FUNCTION} // definice jedné hlavičky funkce skládajíc se z FUNCTION ID ():bool;
			
			; // <-- It's a kind of magic!
	

			if(cover(FUNCTION) == FAIL)
				return FAIL;

			PRD("\t - new F (%s)\n", strGetStr(&attr));
			tHtabItem * item = htabFind(globalSTable, &attr);
			if(item != NULL){ // existuje
				PRD("\t - new F found(%s)\n",strGetStr(item->key));

				if(item->data->type != T_FUNC){ // existuje ale ne jako fce
					ecode = ERR_SEM_DEF;
					return FAIL;
				}

				PRD("\t - fce (%s) defined?\n", strGetStr(item->key));
				if(item->data->isDef == DEFINED){ // už definovaná
					PRD("\t - fce už defined(%s)\n", strGetStr(item->key));
					ecode = ERR_SEM_DEF;
					return FAIL;
				} 

				actSTable = listGetLast(item->data->val.head);
				PRD("ACT<-HEAD: %p\n", (void*)actSTable);
				
				actFunSym = item;
			} else { // nexistuje  definujeme (nebo deklarujeme?)

				// create LTS , create head a vložit LTS do head
				
				tHtab * lSTable;
				lSTable = htabInit(HTAB_SIZE);
				actSTable = lSTable;
				PRD("ACT<-LTS: %p\n", (void*)actSTable);


				tSymbol * symbol; // záznam o fci v globální TS
				symbol = (tSymbol*) malloc(sizeof(tSymbol));

				if(htabInsert(globalSTable, strClone(&attr), symbol, &actFunSym) == HT_REDEF){
					ecode = ERR_SEM_DEF;
					return FAIL;
				}

				tList * head;
				head = (tList*) malloc(sizeof(tList));
				listInit(head);
				symbol->val.head = head;
				symbol->type = T_FUNC;
				symbol->isDef = UNDEF;

				listPush(head, lSTable);

				listActLast(head);
			}

			return cover(ID) || cover(L_PARENTH) || funcHeadParamsEnd();
	}
	return FAIL;
}

#define resetParamCounter (paramCounter = 0, SUCCESS)

bool funcHeadParamsEnd(){
	switch(token){
		case R_PARENTH: // <funcHeadParamsEnd> -> ) : <funcHeadType> { ) } 
			return cover(R_PARENTH) || cover(COLON) || funcHeadType();
		case ID: // <funcHeadParamsEnd> -> <paramsList> ) : <funcHeadType>  {ID}  
			return resetParamCounter || paramsList() || cover(R_PARENTH) || cover(COLON) || funcHeadType();
	}
	return FAIL;
}

bool funcHeadType(){
	switch(token){
		case TYPE:

			;
			tSymbol * symbol = (tSymbol*) malloc(sizeof(tSymbol)); // proměnná v LTS sloužící k uložení návratové hodnoty fce

			tHtabItem * item; // nově vzniklá položka v TS
			
			PRD("---- funkcia %s defStav: %d\n",strGetStr(actFunSym->key), actFunSym->data->isDef);
			if(actFunSym->data->isDef == DECLARED){ // už vloženo => byl forward, kontrola zda sedí
				if(((tHtabItem*)listGetFirst(actFunSym->data->val.head))->data->type != getType(&attr)){ 
					ecode = ERR_SEM_DEF;
					return FAIL;
				}

			} else if(actFunSym->data->isDef == UNDEF) {

				if(htabInsert(actSTable, actFunSym->key, symbol, &item) == HT_REDEF){
					ecode = ERR_SEM_DEF;
					PRD("%d\n", actSTable == globalSTable );
					return FAIL;
				}

				listPushFirst(actFunSym->data->val.head, item);

				symbol->type = getType(&attr);
				
			} else { // DEFINED - sem by to nemělo djít
				ecode = ERR_SEM_DEF;
				return FAIL;
			}


			return cover(TYPE) || cover(SEMICOLON);
	}
	return FAIL;
}

bool paramsList(){
	switch(token){
		case ID: //<paramsList> -> <param> <paramsListEnd> {ID}
			return param() || paramsListEnd();
	}
	return FAIL;
}

bool paramsListEnd(){
	switch(token){
		case SEMICOLON: //<paramsListEnd> -> ; <paramsList> {;}
			return  cover(SEMICOLON) || paramsList();
		default: //<paramsListEnd> -> eps {eps}
			return SUCCESS;
	}
}

bool param(){
	paramCounter++;

	switch(token){
		case ID:// <param> -> ID : TYPE  {ID}
			;

			tHtabItem * item;

			if(actFunSym->data->isDef == DECLARED){ // deklarováno, byl forward

				tList * head = actFunSym->data->val.head;

				listActFirst(head);
				item = listGetFirst(head);
								
				int i = 0;
				while(item != listGetLast(head)){
					
					if(paramCounter == i){
						
						item = listGetAct(head);
						
						if(strCmpString(item->key, &attr)!=0 ){
							ecode = ERR_SEM_DEF;
							return FAIL;
						} 
						if(cover(ID) == FAIL || cover(COLON) == FAIL)
							return FAIL;

						if(item->data->type != getType(&attr)){
							ecode = ERR_SEM_DEF;
							return FAIL;
						} 
						break;
					}
					listActNext(head);
					i++;
				}

			} else  if(actFunSym->data->isDef == UNDEF) {

				// vytvořit param v LTS 
				
				tSymbol * symbol = (tSymbol*) malloc(sizeof(tSymbol));
				symbol->isDef = UNDEF;

				if(htabInsert(actSTable, strClone(&attr), symbol, &item) == HT_REDEF){
					ecode = ERR_SEM_DEF;
					return FAIL;
				}
				listPushBeforeAct(actFunSym->data->val.head, item);
				if(cover(ID) == FAIL || cover(COLON) == FAIL )
					return FAIL;

				if(token != TYPE)
					return FAIL;

				symbol->type = getType(&attr);
				
			} else { // defined (sem by to nemělo nikdy dojít)
				ecode = ERR_SEM_DEF;
				return FAIL;
			}



			return cover(TYPE);
	}
	return FAIL;
}

bool funcBody(){
	switch(token){
		case BEGIN: // <funcBody> -> <commandBlock> ; {BEGIN}

			generateInstr(I_LABEL, actFunSym->key, NULL, NULL);

			if(commandBlock() == FAIL)
				return FAIL;

			if(cover(SEMICOLON) == FAIL)
				return FAIL;


			generateInstr(I_RETURN, actFunSym->key, NULL, NULL);

			actFunSym->data->isDef = DEFINED;
			return SUCCESS;
	}
	return FAIL;
}

bool funcHeadEnd_body(){
	switch(token){
		case FORWARD: // <funcHeadEnd_body> ->  FORWARD ; { FORWARD }

			actFunSym->data->isDef = DECLARED;

			return cover(FORWARD) || cover(SEMICOLON);

		case VAR: // <funcHeadEnd_body> -> <varsDef> <funcBody> {VAR}
			return varsDef() || funcBody();
		case BEGIN: // <funcHeadEnd_body> -> <funcBody> {BEGIN}
			return funcBody();
	}
	return FAIL;
}

bool body(){
	switch(token){
		case BEGIN: // <body> -> <commandBlock> . {BEGIN}
			generateInstr(I_LABEL, dollarLabel, NULL, NULL);
			return commandBlock() || cover(DOT) ;
	}
	return FAIL;
}

bool commandBlock(){
	switch(token){
		case BEGIN: // <commandBlock> -> BEGIN <comandBlockEnd> {BEGIN}
			return cover(BEGIN) || commandBlockEnd();
	}
	return FAIL;
}

bool commandBlockEnd(){
	switch(token){
		case END: //	<comandBlockEnd> ->  END {END}
			return cover(END);
		case ID:
		case INT:
		case REAL:
		case BOOL:
		case STRING:
		case L_PARENTH:
		case IF:
		case WHILE:
		case REPEAT:
		case WRITE:
		case READLN:
		case BEGIN: //	<commandsList> -> <command> <commandsListEnd> {INT, REAL, BOOL, STRING, ID, (, IF, WHILE, REPEAT, BEGIN}  // seznam příkazů, tedy něco ; něco ; něco (bez středníku za posledním) , složený z jednoho příkazu a zbytku seznamu příkazů
			return commandsList() || cover(END);
	}
	return FAIL;
}

bool commandsList(){
	switch(token){
		case ID:
		case INT:
		case REAL:
		case BOOL:
		case STRING:
		case L_PARENTH:
		case IF:
		case WHILE:
		case REPEAT:
		case WRITE:
		case READLN:
		case BEGIN: //	<commandsList> -> <command> <commandsListEnd> {INT, REAL, BOOL, STRING, ID, (, IF, WHILE, REPEAT, BEGIN}
			return command() || commandsListEnd();
		default: //	<commandsList> -> eps {eps}
			return FAIL; //return SUCCESS;
	}
}

bool commandsListEnd(){
	switch(token){
		case SEMICOLON: // <commandsListEnd> -> ; <commandsList> {;}
			return cover(SEMICOLON) || commandsList();
		default: // <commandsListEnd> -> eps {eps} 
			return SUCCESS;
	}
}

#define forgetType (actType = T_UNDEF, SUCCESS)
#define isBool ( lastItem->data->type != T_BOOL ? (ecode = ERR_SEM_TYPE, FAIL) : SUCCESS )

bool command(){
	switch(token){
		case BEGIN: // <command> -> <commandBlock> {BEGIN}
			return commandBlock() || forgetType;
		case ID: // <command> -> ID := <expr> {ID}
			return assign() || forgetType;
		case IF: // <command> -> IF <expr> THEN <commandBlock> {IF} 
			return ifcommand();
		case WHILE: // <command> -> WHILE <expr> DO <commandBlock> {WHILE} 
			return whilecommand();
		case REPEAT: // <command> -> REPEAT <commandsList> UNTIL <expr> {REPEAT}
			return repeatcommand();
		case READLN: // <command> -> READLN ( ID ) 
			return cover(READLN) || readlncall() || forgetType;
		case WRITE: // <command> -> WRITE ( <argsList> )
			return cover(WRITE) || writecall();
	}
	return FAIL;
}


bool repeatcommand(){

	if(cover(REPEAT) == FAIL)
		return FAIL;

	string * label = getUniKey();
	generateInstr(I_LABEL, label, NULL, NULL);

	if(commandsList() == FAIL)
		return FAIL; 

	if(cover(UNTIL) == FAIL)
		return FAIL;

	if(expr(false) == FAIL)
		return FAIL;

	if(isBool == FAIL)
		return FAIL;

	if(forgetType == FAIL)
		return FAIL;

	generateInstr(I_JMPE, label, lastItem->key, NULL);


	return SUCCESS;
}

bool whilecommand(){

	if(cover(WHILE) == FAIL){
		return FAIL;
	}

	string * startlabel = getUniKey();
	string * endlabel = getUniKey();

	generateInstr(I_LABEL, startlabel, NULL, NULL);


	if(expr(false) == FAIL)
		return FAIL;

	if(isBool == FAIL)
		return FAIL;


	actType = T_UNDEF;

	generateInstr(I_JMPE, endlabel, lastItem->key, NULL);

	if(cover(DO) == FAIL)
		return FAIL;

	if(commandBlock() == FAIL)
		return FAIL;

	generateInstr(I_JMP, startlabel, NULL, NULL);
	generateInstr(I_LABEL, endlabel, NULL, NULL);


	return SUCCESS;   
}

bool ifcommand(){

	if(cover(IF) == FAIL)
		return FAIL;
	PP;
	if(expr(false) == FAIL)
		return FAIL;
	PP;
	if(isBool == FAIL)
		return FAIL;

	actType = T_UNDEF;

	if(cover(THEN) == FAIL)
		return FAIL;
	
	
	string * iflabel = getUniKey();
	string * elselabel = getUniKey();

	generateInstr(I_JMPE, iflabel, lastItem->key, NULL);

	if(commandBlock() == FAIL)
		return FAIL;

	if(cover(ELSE) == FAIL)
		return FAIL;

	generateInstr(I_JMP, elselabel, NULL, NULL);
	generateInstr(I_LABEL, iflabel, NULL, NULL);

	if(commandBlock() == FAIL)
		return FAIL;

	generateInstr(I_LABEL, elselabel, NULL, NULL);


	return SUCCESS;
}


/* přiřazení */
bool assign(){ // <command> -> ID := <expr> {ID}
	if(token != ID){ // syntax check 
		return FAIL;
	}
	// sem check:
	PRD("hledam ID (%s)\n", strGetStr(&attr)); 
	
	tHtabItem * item = htabFind(actSTable, &attr);
	if(item == NULL){ // symbol nedefinovan
		item = htabFind(globalSTable, &attr);
		if(item == NULL){
			PRD("ASSIGN TO UNDEFINED ID\n");
			ecode = ERR_SEM_DEF;
			return FAIL;
		}
	}

	actType = item->data->type;
	// sem check end

	if (cover(ID) == FAIL)
		return FAIL;

	if (cover(ASSIGN) == FAIL)
		return FAIL;

	if(expr(true) == FAIL){
		return FAIL;
	}

	if (actType != lastItem->data->type){ // tato kontrola nutná!  
		ecode = ERR_SEM_TYPE;
		return FAIL;
	}

	PRD("> assign (%s == %s)\n", strGetStr(item->key), strGetStr(lastItem->key));
	generateInstr(I_MOV, item->key,lastItem->key,NULL);

	return SUCCESS;
}

/* volání příkazu write */
bool readlncall(){
	PRD("> readlncall\n");
	if(cover(L_PARENTH) ==  FAIL)
		return FAIL;

	if(token != ID)
		return FAIL;

	tHtabItem *item = htabFind(actSTable, &attr);
	if(item == NULL){
		ecode = ERR_SEM_DEF;
		return FAIL;
	}

	if(item->data->type == T_BOOL){
		ecode = ERR_SEM_TYPE;
		return FAIL;
	}

	generateInstr(I_READ,item->key,NULL,NULL);
	// todo generovat instrukci

	return  cover(ID) || cover(R_PARENTH);
}

/* volání příkazu write */
bool writecall(){
	PRD("> writecall\n");

	if(cover(L_PARENTH) == FAIL)
		return FAIL;

	/*if(token == R_PARENTH){
		return FAIL;
	}
	*/

	while(1){
		if(expr(true) == FAIL){
			return FAIL;
		}

		generateInstr(I_WRITE, lastItem->key, NULL, NULL);

		actType = T_UNDEF;

		if(token == R_PARENTH) // skok na konec volání write
			break;

		if(token == COMMA){
			if(cover(COMMA) == FAIL) // token těsně za , čárkou
				return FAIL; 

			actType = T_UNDEF;
		} // cyklíme dál
		else // něco jineho než čárka => syntax err .. např fn(1+2end)
			return FAIL;
	}


	if(cover(R_PARENTH) == FAIL)
		return FAIL;

	return SUCCESS;
}


/* zpracování výrazů */

// pravidla pro E -> 
#define ERULES_SIZE 16



tToken Erules[ERULES_SIZE][4] = { 
	[R_ID] = {ID,STOP},
	[R_INT] = {INT,STOP}, // E->INT
	[R_REAL] = {REAL,STOP},
	[R_BOOL] = {BOOL,STOP},
	[R_STRING] = {STRING,STOP},
	[R_PAR] = {L_PARENTH,E,R_PARENTH,STOP}, // E -> ( E )
	[R_PLUS] = {E,PLUS,E,STOP}, // E -> E + E
	[R_MINUS] = {E,MINUS,E,STOP},
	[R_MUL] = {E,MUL,E,STOP},
	[R_DIV] = {E,DIV,E,STOP},
	[R_EQU] = {E,EQU,E,STOP},
	[R_NON_EQU] = {E,NON_EQU,E,STOP},
	[R_LESS] = {E,LESS,E,STOP},
	[R_MORE] = {E,MORE,E,STOP},
	[R_LESS_EQU] = {E,LESS_EQU,E,STOP},
	[R_MORE_EQU] = {E,MORE_EQU,E,STOP},
};

#define listPushAfterAct(x,y) do { /*printf("ppa(%p)\n",(void*)y); */listPushAfterAct(x,(void*)(y));} while(0)
#define listPush(x,y) do { /*printf("push(%p)\n",(void*)y);*/ listPush((x),(void*)(y));} while(0)
//#define listPop(x) do { /*printf("pop(%p)\n",*/ listPop(x) /*)*/ ;} while(0)

bool expr(bool isParam){  // TODO implementovat pomocí precedenční analýzy
	PRD("> expr\n");

	bool exprEmpty = true;

	string varName; // sem se bude ukládat jméno proměnné... a nebo str hodnota konstanty
	strInit(&varName);

	tList stack;
	listInit(&stack);

	tList rstack;
	listInit(&rstack); // pomocný stack pro redukování

	tList estack;
	listInit(&estack); // pomocný stack pro ukládání itemů...


	tPrTableOP op;
	tToken top;
	tToken pseudoDollar; // když je to větší jak dolar, je to dolar
	listPush(&stack, DOLLAR);
	listActLast(&stack);

	tListItem *item; // pomocné pro procházení při hledání pravidel pro redukci


	while( 1 ) {

		top = (tToken) listGetAct(&stack); //vrchol zásobníku
		
		if(token > DOLLAR){
			pseudoDollar = token; // zaloha 
			token = DOLLAR;
		}

		if(top == DOLLAR && token == DOLLAR) // konec zpracování výrazů!
			break;

		op = prTable[top][token]; // < nebo > nebo = ...

		//PRD("op(%d)(%d)\n", top, token);

		if(token == ID || token == BOOL || token == INT || token == REAL || token == STRING)
			strCopyString(&varName, &attr);

		switch(op){
			case _LESS: // < shift
				listPushAfterAct(&stack, RED);
				listPush(&stack, token);
				listActLast(&stack);
				if(cover(ANY) == FAIL){ // načíst další token
					PRD("\t - expr cover FAILED\n");
					return FAIL;
				} 
				break;
			case _MORE: // > redukce
				// projít zprava, najít < RED, redukovat

				// zkopírovat redukované věcičky do pomocného stacku // otáčí pořadí
				item = listGetPtrLast(&stack);
				while(item->data != (void*)RED){ 
					listPush(&rstack, item->data);
					item = listGetPtrPrev(item);
				}
				// najít pravidlo (jeho index)
				bool ruleFound = false;
				tRuleIndex rIndex = 0;
				for (; rIndex < ERULES_SIZE; rIndex++){
					item = listGetPtrLast(&rstack); // projít rstack a porovnávat
					for(int i = 0; ; i++){
						if((void*)Erules[rIndex][i] == (void*)STOP){
							// win, rule found
							ruleFound = true;
							exprEmpty = false;
							// popnout i×
							while(i--){
								listPop(&stack);
							}
							listPop(&stack); // pop <

							// TODO podle rIndex udělat něco (generovat intrukce?)
							
							// semantická kontrola pro redukční pravidla 0-4
							tHtabItem * item;
							tSymbol * symbol;
							switch(rIndex){
								case R_ID: // ID


									item = htabFind(actSTable, &varName);
									if(item == NULL){ // symbol nedefinovan
										item = htabFind(globalSTable, &varName);
										if(item == NULL){
											ecode = ERR_SEM_DEF;
											return FAIL;
										}
									}

									listPush(&estack, item); // uložení typu

									if(actType == T_UNDEF)
										actType = item->data->type;
									
									break;
									
								case R_BOOL:
								case R_STRING:
								case R_INT: // INT
								case R_REAL:

									// vložit do TS konstantu
									symbol = (tSymbol*) malloc(sizeof(tSymbol));
									string * key = getUniKey();
									
									// uložení honstantní hodnoty
									switch(rIndex){
										case R_BOOL:
											symbol->val.boolval = strCmpConstStr(&varName, "true") ? false : true;
											symbol->type = T_BOOL;
											break;
										case R_STRING:
											symbol->val.stringval = strClone(&varName);
											symbol->type = T_STRING;
											toString(symbol->val.stringval);
											break;
										case R_INT:
											if(toInt(&varName, &symbol->val.intval) != OK){
												ecode = ERR_RUN_OTHER;
												return FAIL;
											}
											symbol->type = T_INT;
											break;
										case R_REAL:
											if(toDouble(&varName, &symbol->val.realval) != OK){
												ecode = ERR_RUN_OTHER;
												return FAIL;
											}
											symbol->type = T_REAL;
											break;
									}
									symbol->isDef = DEFINED;
									
									tHtabItem * item;
									htabInsert(actSTable, key, symbol, &item);

									listPush(&estack, item);

									// typová kontrola
									if(actType == T_UNDEF)
										actType = symbol->type;

									break;
									// typová kontrola oparcí
								case R_PLUS:
								case R_MINUS:
								case R_MUL:
								case R_DIV:
								case R_EQU:
								case R_NON_EQU:
								case R_MORE:
								case R_LESS:
								case R_MORE_EQU:
								case R_LESS_EQU:
									; // <= nemazat  !

									// vzít dva typy a zkontrolovat...
									tHtabItem * b = listPop(&estack);
									tHtabItem * a = listPop(&estack);  //přetočené protože pop (viz. 4/2)

									tHtabItem * r; //vysledek
	 								
									tType rtype = getResultType(a, b, rIndex); //typ výsledku

									if(a->data->type != rtype && rIndex <= R_DIV){ // přetypovat
										tHtabItem  * ra = createTempItem(rtype);
										// todo generovat instrukci přetypování (a ra)
										PRD("\t retype a %s\n", strGetStr(a->key));
										generateInstr(I_INT_TO_REAL, ra->key, a->key, NULL);
										a = ra;
									}
									if(b->data->type != rtype && rIndex <= R_DIV){ // přetypovat
										tHtabItem  * rb = createTempItem(rtype);
										// generovat instrukci přetypování (b rb)
										PRD("\t retype b %s\n", strGetStr(b->key));
										generateInstr(I_INT_TO_REAL, rb->key, b->key, NULL);
										b = rb;
									}

									r = createTempItem(rtype); // místo pro výsledek
									// todo generovat isntrukci operaci (a b r)
									
									PRD("1. param %s, 2. param %s, 3. param %s\n",strGetStr(r->key),strGetStr(a->key),strGetStr(b->key));
									generateInstr(getInstrType(rIndex), r->key, a->key, b->key);
									 
									listPush(&estack, r);

								// todo vytvořit místečko pro návratovou hodnotu operace
								// generovat intrukci operace

							}// konec semantické kontroly

							listActLast(&stack);
							listPush(&stack,E);
							break;
						}
						if((void*)Erules[rIndex][i] != item->data){
							break; // tento rule to není, zkusit další
						}
						item = listGetPtrPrev(item);
					}
					if(ruleFound){
						PRD("\t - rule found! (%d)\n", rIndex);
						break;
					}
				}
				if(!ruleFound){
					ecode = ERR_SYNTAX; 
					PRD("\t - rule not found\n");
					return FAIL;
				}

				break;
			case _EQU:
				listPush(&stack, token);
				listActLast(&stack);
				if(cover(ANY) == FAIL){// načíst další token
					PRD("\t - equ any cover FAILED\n");
					return FAIL;
				} 
				break;
			case _FNC:
				exprEmpty = false;
				if(fncall(&stack, &estack, &varName) == FAIL){
					PRD("\t - fncall failed\n");
					return FAIL;
				}
				PP;
				break;
			case _ERR:

				PRD(":7\n");
				if(isParam && token == R_PARENTH ){
					if(!exprEmpty){
						tHtabItem * erItem = ((tHtabItem*)listPop(&estack)); // navratová hodnota celeho expru
						PRD(":8\n");
						lastItem = erItem;
						return SUCCESS;
					}else{

						return FAIL;
					}
				}
				PRD("\t - err prTable\n");

				return FAIL;
				break;
		}

	}
	token = pseudoDollar;

	PZ;
	//strFree(&varName);
	//listFree(&stack);
	//listFree(&rstack);

	if(exprEmpty){
		return FAIL;
	}

	PRD("\t - actType(%d)\n",actType);

	tHtabItem * erItem = ((tHtabItem*)listPop(&estack)); // navratová hodnota celeho expru

	PRD("\t - exprEmpty(%d)\n",exprEmpty);

	//listFree(&estack);
	lastItem = erItem;

	return  SUCCESS;
}

/* volání funkce */
bool fncall(tList * stack,  tList * estack, string * varName){


	// ted je token == ID

	// kontroa zda se volá existující fce
	tHtabItem *	item = htabFind(globalSTable, varName);
	if(item == NULL){
		ecode = ERR_SEM_DEF;
		PRD("FUNCTION DOES NOT EXISTS\n");
		return FAIL;
	}

	// typová kontrola
	tList * head = item->data->val.head;

	listActFirst(head);

	tHtabItem * rItem = (tHtabItem*)listGetFirst(head); // navratový hodnota itemu

	tSymbol * rSymbol =  rItem->data; // ukazatel na návratový symbol v LTS fce 

	if(actType == T_UNDEF){
		actType = rSymbol->type; // typ navrácené hodntoy
	}

	// řešení parametrů
	tType prevActType = actType;
	actType = T_UNDEF;

	if(cover(L_PARENTH) == FAIL) // načíst další token za '('
		return FAIL;
	
	generateInstr(I_MAKE_FRAME, listGetLast(head), NULL, NULL);
	PRD("LTS: %p\n", listGetLast(head));


	int headIndex = 1;

 	switch(token){
 		default:
 			while(1){ // cyklíme přes argumenty		
 				if(expr(true) == FAIL)	// zpracování jednoho argumentu
 					return FAIL;

 				listActFirst(head);	

				
				for(int i = 0; i < headIndex; i++ ){
					listActNext(head);
				}
 				
 				if(listGetAct(head) == listGetLast(head)){ // víc argumentů než params (last už je LTSptr a ne param)
 					PRD("TOO MANY PARAMS\n");
 					ecode = ERR_SEM_TYPE;
 					return FAIL;
 				}

 				PRD(":10\n");
 				tSymbol * pSymbol = ((tHtabItem*)listGetAct(head))->data; // ukazatel na parametr fce
 				PRD(":10.1\n");
				if(actType != pSymbol->type){
					PRD("BAD ARG TYPE\n");
					ecode = ERR_SEM_TYPE;
					return FAIL;
				}

				// kam <- co
				generateInstr(I_SET_PAR, ((tHtabItem*)listGetAct(head))->key, lastItem->key, NULL);


 				if(token == R_PARENTH) // skok na konec volání fce (další case)
 					break;

 				if(token == COMMA){
 					if(cover(COMMA) == FAIL) // token těsně za , čárkou
						return FAIL; 
					actType = T_UNDEF;
 				} // cyklíme dál
 				else // něco jineho než čárka => syntax err .. např fn(1+2end)
 					return FAIL;	//		

 				headIndex++;							  
 			}

 		case R_PARENTH: //  ')' konec volání fce
			
			listActNext(head);
			if(listGetAct(head) != listGetLast(head)){ // méně argumentů než params (poslední v head je LTSptr)
				PRD("TOO FEW PARAMS\n");
				ecode = ERR_SEM_TYPE;
				return FAIL;
			}

 			// call fn();

			string * funcallendlabel = getUniKey();

			tHtabItem * r = createTempItem(rSymbol->type);
 			
 			generateInstr(I_CALL, ((tHtabItem*)listGetFirst(head))->key, funcallendlabel, r->key); 
 			generateInstr(I_LABEL, funcallendlabel, NULL, NULL);
 			 
 			// 
			listPop(stack); // ID
			listPop(stack); // <
			listActLast(stack);
			listPush(stack,E); // vysledek volání fce

			listPush(estack,r);

			PRD("\t - return type(%d)\n",rItem->data->type);


			if(cover(R_PARENTH) == FAIL) // token těsně za závorkou ')'
				return FAIL; 
			PRD("\t - fn called\n");

			actType = prevActType;

			return SUCCESS;

 	}
	
	return FAIL;
}



/**
 * Pomocné fce
 * 
 */

/**
 *  vloží interní funkci do GTS
 *  id fce
 *  návratový typ fce
 *  počet parametrů
 *  nazev parametru
 *  typy parametrů
 */

void insertInternFunc(char * fname, tType type, int len,  ...){

	tHtab * lSTable;
	lSTable = htabInit(HTAB_SIZE);
	PRD("LTS: %p\n", (void*)lSTable);

	tSymbol * symbolG = (tSymbol*) malloc(sizeof(tSymbol));
	symbolG->type = T_FUNC;

	string * key = strCreate(fname);

	htabInsert(globalSTable, key, symbolG, NULL);

	tList * head  = (tList*) malloc(sizeof(tList));
	listInit(head);
	symbolG->val.head = head;
	symbolG->isDef = DEFINED;



	tHtabItem * item;
	// vložení return var	

	tSymbol * pSymbol = malloc(sizeof(tSymbol));

	pSymbol->type = type;
	pSymbol->isDef = UNDEF;

	htabInsert(lSTable, key, pSymbol , &item);
	listPush(head, (void*)item);


	// vložení případných parametrů
	va_list p;
	va_start(p, len);
	for (int i = 0; i < len; ++i){

		// itemy nejsou v hashtable, nemusí mít key
		tSymbol * pSymbol = malloc(sizeof(tSymbol));

		string * key = strCreate(va_arg(p, char*));

		pSymbol->type = va_arg(p, tType);
		pSymbol->isDef = UNDEF;

		htabInsert(lSTable, key, pSymbol , &item);
		listPush(head, (void*)item);

		PRD("fake fn(%s) param(%s) \n", fname, strGetStr(key));
	}

	va_end(p);

	listPush(head, lSTable); // přidat do head LTS

}


/* generování unikátního klíče */
int uniKeyCounter = 0;

const int CHARS_LEN = 38;
const char chars[] = "0123456789qwertzuioplkjhgfdsayxcvbnm_-";

string * getUniKey(){
	int i = uniKeyCounter;

	string * key = strCreate("$");
	while(i){
		strAddChar(key, chars[i%CHARS_LEN]);
		i = (i - (i%CHARS_LEN))/CHARS_LEN;
	}

	uniKeyCounter++;
	return key;
}	



bool areAllFncsDef() {
	PRD("> areAllFncsDef\n");
	tHtabItem *item;
	for(int i = 0 ; i < globalSTable->size; i++){
		item = globalSTable->data[i];
		while(item != NULL){
			if(item->data->type == T_FUNC && item->data->isDef != DEFINED){
				PRD("\t - %s error\n", strGetStr(item->key));
				ecode = ERR_SEM_DEF;
				return FAIL;		
			}
			item = item->next;	
		}
	}
	return SUCCESS;
}



tHtabItem * createTempItem(tType type){

	tHtabItem * item;

	tSymbol * symbol = (tSymbol*) malloc(sizeof(tSymbol));
	string * key = getUniKey();
	symbol->type = type;
	symbol->isDef = UNDEF;
	
	PRD("> createTempItem: %s\n", strGetStr(key));
	if(htabInsert(actSTable, key, symbol, &item) == HT_REDEF){
		PRD("\t fail, htabinsert");
		printErr(ERR_SEM_DEF);

	}

	return item;
}


tType typeTable[4][4][10] = {
	[T_INT] = {
		[T_INT] = {T_INT,T_INT,T_INT,T_REAL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,},
		[T_REAL] = {T_REAL,T_REAL,T_REAL,T_REAL,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_STRING] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_BOOL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
	},
	[T_REAL] = {
		[T_INT] = {T_REAL,T_REAL,T_REAL,T_REAL,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_REAL] = {T_REAL,T_REAL,T_REAL,T_REAL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,},
		[T_STRING] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_BOOL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
	},
	[T_STRING] = {
		[T_INT] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_REAL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_STRING] = {T_STRING,T_UNDEF,T_UNDEF,T_UNDEF,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,},
		[T_BOOL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
	},
	[T_BOOL] = {
		[T_INT] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_REAL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_STRING] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,},
		[T_BOOL] = {T_UNDEF,T_UNDEF,T_UNDEF,T_UNDEF,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,T_BOOL,},
	},

};


tType getResultType(tHtabItem * a, tHtabItem * b, tRuleIndex op){

	if(a->data->type == T_UNDEF || a->data->type == T_FUNC || b->data->type == T_UNDEF ||b->data->type == T_FUNC ){
		PRD("> getResultType fail 1");
		printErr(ERR_SEM_DEF);
		return T_UNDEF;
	}

	tType newType = typeTable[a->data->type][b->data->type][op];

	if(newType == T_UNDEF){
		PRD("> getResultType fail 2");
		printErr(ERR_SEM_TYPE);
		return T_UNDEF;
	}

	return newType;
}


tType getType(string * attr){

	tType type = T_UNDEF;
	if(strCmpConstStr(attr,"boolean") == 0)
		type = T_BOOL;
	if(strCmpConstStr(attr,"integer") == 0)
		type = T_INT;
	if(strCmpConstStr(attr,"real") == 0)
		type = T_REAL;
	if(strCmpConstStr(attr,"string") == 0)
		type = T_STRING;

	return type;
}

tInstr * generateInstr(int type, void *pKey1, void *pKey2, void *pKey3){
	PRD("> generateInstr type: %d\n", type);

	tInstr * newInstr = malloc(sizeof (tInstr));
	
	newInstr->instType = type;
	newInstr->addr1 = pKey1;
	newInstr->addr2 = pKey2;
	newInstr->addr3 = pKey3;

	listPush(globalInstrList, newInstr);

	return newInstr;
}

int getInstrType(tRuleIndex rule){
	switch(rule){
		case R_PLUS:
			return I_ADD;
		break;
		case R_MINUS:
			return I_SUB;
		break;
		case R_MUL:
			return I_MUL;
		break;
		case R_DIV:
			return I_DIV;
		break;
		case R_EQU:
			return I_ISEQUAL;
		break;
		case R_NON_EQU:
			return I_ISNOTEQUAL;
		break;
		case R_LESS:
			return I_ISBELOW;
		break;
		case R_MORE:
			return I_ISABOVE;
		break;
		case R_LESS_EQU:
			return I_ISEQBELOW;
		break;
		case R_MORE_EQU:
			return I_ISEQABOVE;
		break;
	}

	return FAIL;
}