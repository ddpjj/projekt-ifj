/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <ctype.h>
#include "str.h"
#include "scanner.h"

// výčet (nekoncových) stavů automatu 
enum states {
	S_INIT,
	S_COMMENT,
	S_MORE,
	S_LESS,
	S_COLON,

	S_ID,

	S_NUM,
	S_REAL,
	S_REAL_X,
	S_REAL_E,
	S_REAL_SIGN,
	S_REAL_E_NUM,

	S_STRING,
	S_STRING_2,
	S_STRING_LETTER,
	S_STRING_NUMBER,
	S_STRING_NUMBER_X,
	S_STRING_NUMBER_XX,
	S_STRING_NUMBER_1,
	S_STRING_NUMBER_1X,
	S_STRING_NUMBER_1XX,
	S_STRING_NUMBER_2,
	S_STRING_NUMBER_25,
	S_STRING_NUMBER_255,
	S_STRING_NUMBER_2X,
};

// promenna pro ulozeni vstupniho souboru
FILE *source;

void setSourceFile(FILE *f)
{
	source = f;
}


/*
** Vrací token a v některých případech načítá znaky do attr
*/
tToken getNextToken(string *attr){

	strClear(attr);

	int state = S_INIT;
	int c;

	while(1){
		c = getc(source);

		switch(state){

			case S_INIT:

				// bílé znaky ignorovat
				if(isspace(c))
					continue;

				if(c == '/')
					return DIV;
				if(c == '+')
					return PLUS;
				if(c == '-')
					return MINUS;
				if(c == '*')
					return MUL;
				if(c == '(')
					return L_PARENTH;
				if(c == ')')
					return R_PARENTH;
				if(c == ';')
					return SEMICOLON;
				if(c == '.')
					return DOT;
				if(c == ',')
					return COMMA;
				if(c == '=')
					return EQU;
				if(c == EOF)
					return END_OF_FILE;


				if(c == '>'){
					state = S_MORE;
					continue;
				}
				if(c == '<'){
					state = S_LESS;
					continue;
				}
				if(c == ':'){
					state = S_COLON;
					continue;
				}

				if(c == '{'){
					state = S_COMMENT;
					continue;
				}
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue; 
				}
				if(isdigit(c)){
					state = S_NUM;
					strAddChar(attr, c);
					continue;
				}

				if(isalpha(c) || c == '_'){
					state = S_ID;
					strAddChar(attr, c);
					continue;
				}
				
				return LEX_ERR;
				break;


			// { .... }
			case S_COMMENT:
				if(c == '}')
					state = S_INIT;
				if( c == EOF )
					return LEX_ERR;
				continue;
				break;


			// > nebo >=
			case S_MORE:
				if(c == '=')
					return MORE_EQU;

				ungetc(c, source);
				return MORE;
				break;


			// < nebo <= nebo <>
			case S_LESS:
				if(c == '=')
					return LESS_EQU;
				if(c == '>')
					return NON_EQU;

				ungetc(c, source);
				return LESS;
				break;


			// := nebo :
			case S_COLON:
				if(c == '=')
					return ASSIGN;

				ungetc(c, source);
				return COLON;
				break;


			// identifikátor, nebo klíčové slovo
			case S_ID:
				if(isalnum(c) || c == '_'){
					state = S_ID;
					strAddChar(attr, c);
					continue;
				}
				else {
					ungetc(c, source);
					/* a ted dzjistit, zdaje to klíčové slovo či identifikátor */
					
					if(strCmpConstStr(attr, "var") == 0)
						return VAR;
					if(strCmpConstStr(attr, "begin") == 0)
						return BEGIN;
					if(strCmpConstStr(attr, "end") == 0)
						return END;
					if(strCmpConstStr(attr, "then") == 0)
						return THEN;
					if(strCmpConstStr(attr, "do") == 0)
						return DO;
					if(strCmpConstStr(attr, "if") == 0)
						return IF;
					if(strCmpConstStr(attr, "else") == 0)
						return ELSE;
					if(strCmpConstStr(attr, "while") == 0)
						return WHILE;
					if(strCmpConstStr(attr, "repeat") == 0)
						return REPEAT;
					if(strCmpConstStr(attr, "until") == 0)
						return UNTIL;
					if(strCmpConstStr(attr, "function") == 0)
						return FUNCTION;
					if(strCmpConstStr(attr, "forward") == 0)
						return FORWARD;
					if(strCmpConstStr(attr, "readln") == 0)
						return READLN;
					if(strCmpConstStr(attr, "write") == 0)
						return WRITE;
					if(strCmpConstStr(attr, "integer") == 0)
						return TYPE;
					if(strCmpConstStr(attr, "real") == 0)
						return TYPE;
					if(strCmpConstStr(attr, "string") == 0)
						return TYPE;
					if(strCmpConstStr(attr, "boolean") == 0)
						return TYPE;
					if(strCmpConstStr(attr, "true") == 0)
						return BOOL;
					if(strCmpConstStr(attr, "false") == 0)
						return BOOL;
					
					return ID;
				}

				break;


			/* == stavy rozpoznávající číslo INT nebo REAL == */

			case S_NUM:
				if(c >= '0' && c <= '9'){
					state = S_NUM;
					strAddChar(attr, c);
					continue;
				}

				if(c == '.'){
					state = S_REAL;
					strAddChar(attr, c);
					continue;
				}

				if(c == 'E' || c == 'e'){
					state = S_REAL_E;
					strAddChar(attr, c);
					continue;
				}

				ungetc(c, source);
				return INT;
				break;

			case S_REAL:
				if(c >= '0' && c <= '9'){
					state = S_REAL_X;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_REAL_X:
				if(c >= '0' && c <= '9'){
					state = S_REAL_X;
					strAddChar(attr, c);
					continue;
				}

				if(c == 'E' || c == 'e'){
					state = S_REAL_E;
					strAddChar(attr, c);
					continue;
				}

				ungetc(c, source);
				return REAL;
				break;

			case S_REAL_E:
				if(c >= '0' && c <= '9'){
					state = S_REAL_E_NUM;
					strAddChar(attr, c);
					continue;
				}

				if(c == '+' || c == '-'){
					state = S_REAL_SIGN;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_REAL_SIGN:
				if(c >= '0' && c <= '9'){
					state = S_REAL_E_NUM;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_REAL_E_NUM:
				if(c >= '0' && c <= '9'){
					state = S_REAL_E_NUM;
					strAddChar(attr, c);
					continue;
				}

				ungetc(c, source);
				return REAL;
				break;


			/* == Kuníkova část rozpoznávající řetězce == */

			case S_STRING:
				if(c == '\''){
					state = S_STRING_2;
					strAddChar(attr, c);
					continue;
				}

				if(c >= ' '){
					state = S_STRING_LETTER;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_2:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c == '#'){
					state = S_STRING_NUMBER;
					strAddChar(attr, c);
					continue;
				}

				ungetc(c, source);
				return STRING;
				break;

			case S_STRING_LETTER:
				if(c == '\''){
					state = S_STRING_2;
					strAddChar(attr, c);
					continue;
				}

				if(c >= ' '){
					state = S_STRING_LETTER;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER:
				if(c == '0'){
					state = S_STRING_NUMBER;
					strAddChar(attr, c);
					continue;
				}

				if(c == '1'){
					state = S_STRING_NUMBER_1;
					strAddChar(attr, c);
					continue;
				}

				if(c == '2'){
					state = S_STRING_NUMBER_2;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '3' && c <= '9'){
					state = S_STRING_NUMBER_X;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_X:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '0' && c <= '9'){
					state = S_STRING_NUMBER_XX;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_XX:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_1:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '0' && c <= '9'){
					state = S_STRING_NUMBER_1X;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_1X:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '0' && c <= '9'){
					state = S_STRING_NUMBER_1XX;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_1XX:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_2:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '0' && c <= '5'){
					state = S_STRING_NUMBER_25;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '6' && c <= '9'){
					state = S_STRING_NUMBER_2X;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_2X:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_25:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				if(c >= '0' && c <= '5'){
					state = S_STRING_NUMBER_255;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			case S_STRING_NUMBER_255:
				if(c == '\''){
					state = S_STRING;
					strAddChar(attr, c);
					continue;
				}

				return LEX_ERR;
				break;

			/* konec kouzel */

		}


	}


}