/**
 *  Implementace interpretu imperativního jazyka IFJ14
 *
 *  xbalvi00 David Balvín
 *  xbedna55 Drahoslav Bednář 
 *  xbenes38 Jakub Beneš
 *  xkunca07 Jiří Kunčák
 *  xlehne01 Pavla Lehnertová
 *
 */

typedef struct
{
  int instType;  // typ instrukce
  void *addr1; // adresa 1
  void *addr2; // adresa 2
  void *addr3; // adresa 3
} tInstr;

typedef struct listItem
{
  void* data;
  struct listItem *ptrPrev;
  struct listItem *ptrNext;
} tListItem;

typedef struct list
{
  struct listItem *First;	// ukazatel na prvni prvek
  struct listItem *Last;	// ukazatel na posledni prvek
  struct listItem *Act;		// ukazatel na aktivni prvek
} tList;

void listInit(tList *L);
void listFree(tList *L);

int listPush(tList *L, void* ptr);
int listPushFirst(tList *L, void* ptr);
int listPushAfterAct(tList *L, void* ptr);
int listPushBeforeAct(tList *L, void* ptr);

void* listPop(tList *L);
void* listPopFirst(tList *L);
void* listPopAct(tList *L);

int listActive(tList *L);
void listActFirst(tList *L);
void listActLast(tList *L);
void listActNext(tList *L);
void listActPrev(tList *L);
void listActGoto(tList *L, void *instr);

void *listGetPtrFirst(tList *L);
void *listGetPtrLast(tList *L);
void *listGetPtrNext(tListItem* structure);
void *listGetPtrPrev(tListItem* structure);

void* listGetAct(tList *L);
void* listGetFirst(tList *L);
void* listGetLast(tList *L);

//funkce potřebná k testům
void listCharPrint(tList *L);
