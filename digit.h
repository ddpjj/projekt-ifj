/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#define ERR_CONV -1 // Chyba v konverzi (preteceni, poruseni IEEE 754)

int toInt(string *xttr, int *result);
double toDouble(string *xttr, double *result);
