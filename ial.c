/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "gc.h"
#include "ilist.h"
#include "str.h"
#include "ial.h"

#define CI_MASK	32

/** == hash table == **/

/**
 * vrací hash
 * @param  str string
 * @param  htabSize velikost hashtable
 * @return	hash
 */
unsigned int getHash(string *pStr, unsigned htabSize) {
	unsigned int h = 0;
	char *p;
	for(p = strGetStr(pStr); *p!='\0'; p++)
		h = 65599*h + (*p>='A' && *p<='Z') ? (*p|CI_MASK) : (*p);

	return h % htabSize;
}

/**
 * vytvoření a inicializaci tabulky 
 * @param  size velikost
 * @return NULL v případě chyby, jinak ukazatel na hashovací tabulku
 */
tHtab * htabInit(int size) {
	
	tHtab * htab = (tHtab *) malloc(sizeof(tHtab) + sizeof(tHtabItem *) * size);
	if (htab == NULL) 
		return NULL;

	for(int i = 0; i<size; i++)
		htab->data[i] = NULL;
	 
	htab->size = size;
	return htab;
}

/**
 * vyhledávání
 * @param  t ukazatel na tabulku symbolů
 * @param  key string
 * @return	vrací item data, nebo NULL pokud nenalezeno
 */
tHtabItem * htabFind(tHtab *t, string *pKey) {


	unsigned int hash = getHash(pKey, t->size);
	
	tHtabItem * item = t->data[hash];
	while(item != NULL){
		if(strCmpString(item->key, pKey) == 0){
			return item; // nalezeno
		}
		item = item->next;
	}

	return NULL; 	// nenalezeno	
}

/**
 * vloží prvek
 * @param  t hash table
 * @param  key string
 * @param  data záznam symbolu
 * @return HT_OK nebo HT_REDEF pokud už symbol existuje, HT_ERR v případě chyby
 */
int htabInsert(tHtab *t, string *pKey, void *data, tHtabItem ** newItem ) {

	unsigned int hash = getHash(pKey, t->size);

	tHtabItem * item = t->data[hash];
	
	while(item != NULL){
		if(strCmpString(item->key, pKey) == 0){			
			return HT_REDEF; // již existuje
		}
		item = item->next;	
	}
	// neexistuje vkládáme

	item = (tHtabItem *) malloc(sizeof(tHtabItem)); 
	if(item == NULL){
		return HT_ERROR;
	}

	item->key = pKey;

	item->data = data;
	item->next = t->data[hash];

	t->data[hash] = item;

	if(newItem != NULL)
		*newItem = item;
	 
	return HT_OK;
}

/**
 * zrušení celé tabulky 
 * @param t tabulka symbolů
 */
void htabFree(tHtab *t) {
	tHtabItem *item;
	tHtabItem *next;
	for(int i = 0 ; i < t->size; i++){
		item = t->data[i];
		while(item != NULL){
			next = item->next;	
			//strFree(&item->key);
			free(item);
			item = next;
		}
	}
	free(t);
}

tSymbol * symClone(tSymbol * sym) {
	tSymbol * newSym = (tSymbol *) malloc(sizeof(tSymbol)); 

	newSym->isDef = sym->isDef;
	newSym->type = sym->type;

	if (newSym->type == T_STRING && newSym->isDef == DEFINED){
		newSym->val.stringval = strClone(sym->val.stringval);
	}else{
		newSym->val = sym->val;
	}

	return newSym;
}

tHtab * htabClone(tHtab *t){
	tHtab * newhtab;
	tHtabItem *item;
	newhtab = htabInit(t->size);
	for(int i = 0 ; i < t->size; i++){
		item = t->data[i];
		while(item != NULL){
			tSymbol * tempSymbol = symClone(item->data);	
			htabInsert(newhtab, item->key, tempSymbol, NULL);
			item = item->next;
		}
	}

	return newhtab;
}


/* == Merge sort ==  */
void merge(char *s1, int left, int right, int mid);
void mergeSort(char *s1, int left, int right);

/* základní volání funkce kde se upraví vstup našeho STRINGu */
void sort(string *s1, string *s2){
	int left = 0;
	int right = strGetLength(s1) - 1;
	strClear(s2);
    strCopyString(s2, s1);
    char* teststring;
    teststring = strGetStr(s2);

	mergeSort(teststring, left, right);

	return;
}

/* samotný mergesort který prochází řetězec */
void mergeSort(char *s1, int left, int right){
	int mid = (left + right)/2;

	if(left < right){
		mergeSort(s1, left, mid);
		mergeSort(s1, mid+1, right);
		merge(s1, left, right, mid);
	}

	return;
}

/* slepování jednotlivých dílků */
void merge(char *s1, int left, int right, int mid){
	char tempString[right-left+1];
	int pos = 0;
	int lpos = left;
	int rpos = mid+1;
	char l,r;

    while(lpos <= mid && rpos <= right){
            l = s1[lpos];
            r = s1[rpos];
            if(l <= r){
                    tempString[pos] = s1[lpos];
                    pos++;
                    lpos++;
            }else{
                    tempString[pos] = s1[rpos];
                    pos++;
                    rpos++;
            }
    }

    while(lpos <= mid){
    	tempString[pos] = s1[lpos];
    	pos++;
    	lpos++;
    }
    
    while(rpos <= right){
    	tempString[pos] = s1[rpos];
    	pos++;
    	rpos++;
    }


    for(int i = 0;i < pos; i++){
            s1[i+left] = tempString[i];
    }

    return;
}

int find(string *s, string *search){

	char * str1 = strGetStr(s);
	char * str2 = strGetStr(search);
	int jump = strGetLength(search);
	int limit = strGetLength(s);


	if (jump == 0) {
		return 1; // Pokud je retezec prazdny
	}

	int index; // Index str1
	int inx_loop; // Index loop
	int inx_jump; // Index jumpu
	int found; // Pomocna promena pro hledani
	int CharJump[strGetLength(search)];

	for (inx_jump = 0; inx_jump < jump; inx_jump++) {
		CharJump[inx_jump] = jump - inx_jump - 1;
	}

	index = jump - 1;
	while (index <= limit) {
		inx_jump = jump - 1; found = 0;
		inx_loop = 0;
		while (inx_loop <= jump-1) {
			if (found == 0) {
				if (str1[index - inx_loop] != str2[jump - inx_loop - 1]) {
					found = 1;
				}
				else if (inx_loop == jump - 1) {
					return index - inx_loop + 1; // Nalezeno
				}
			}
			if (found == 1) {
				if (str1[index] == str2[jump - inx_loop - 1]) {
				found = 2;
				}
				inx_jump = inx_loop;
			}
			inx_loop++;

		}

		if (found == 1) {
			index += jump;
		}
		else {
			index += CharJump[jump - inx_jump - 1];
		}
	}
	return 0; // Nenalezeno
}


int length(string *s1){
    toString(s1);
    return strGetLength(s1);
}

void copy(string *s1, string *s2, int startpos, int length){
    toString(s1);
    strClear(s2);

    char* helpstring = strGetStr(s1);
    int pos = startpos - 1;
    if(pos<0){
    	pos = 0;
    }
   	if(pos>length-1){
   		return;
   	}
    while(length > 0 && helpstring[pos] != '\0'){
        strAddChar(s2, helpstring[pos]);
        pos++;
        length--;
    }
    return;
}