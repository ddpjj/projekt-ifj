#	Implementace interpretu imperativního jazyka IFJ14
#
#	xbalvi00 David Balvín
#	xbedna55 Drahoslav Bednář
#	xbenes38 Jakub Beneš
#	xkunca07 Jiří Kunčák
#	xlehne01 Pavla Lehnertová

SOURCES := str.c ilist.c digit.c ial.c scanner.c parser.c gc.c interpret.c ecodes.c
#SOURCES := $(wildcard *.c)
FLAGS := -std=c99 -Wall -Wextra -pedantic -lm

HEADERS := $(wildcard *.h)


all: interpret

d: | setdebug interpret test dotest

setdebug: 
	$(eval FLAGS += -D DEBUG=1)

interpret: main.c $(SOURCES)
	gcc $(FLAGS) -o $@ $^

test: tests/test_me.c $(SOURCES)
	gcc $(FLAGS) -o $@ $^

dotest:
	./test

stats:
	$(eva SOURCES += main.c)
	@echo -n "lines: " && wc -l $(SOURCES) $(HEADERS) | tail -n 1 | sed -r "s/[ ]*([0-9]+).*/\1/g"
	@echo -n "Size of code: " && du -hsc $(SOURCES) $(HEADERS) | tail -n 1 | cut -f 1
	@test -e interpret && echo -n "Size of executable: " && du -hs interpret | cut -f 1 || echo "make: Executable not found."

clean:
	$(RM) *.o interpret test

.PHONY: dotest setdebug interpret test d