/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include "ecodes.h"
#define ISGC 1
#include "gc.h"

#define STR_LEN_INC	32

tGC gc;

int gcInit(){
	if((gc.alloclist =  malloc(sizeof(void*) * STR_LEN_INC)) == NULL){
		return GC_ERROR;
	}
	
	for(int i = 0; i < STR_LEN_INC; i++){
		gc.alloclist[i] = NULL;
	}
	
	gc.length = 0;
	gc.allocSize = STR_LEN_INC;
	return GC_OK;
}

void *superMalloc(size_t size){
	if(gc.length >= gc.allocSize){
		if((gc.alloclist =  realloc(gc.alloclist,  (sizeof(void*) * (gc.allocSize + STR_LEN_INC)))) == NULL){
			printErr(ERR_INTERNAL);
			return NULL;
		} 
		
		for(int i = gc.allocSize; i < (gc.allocSize + STR_LEN_INC); i++){
			gc.alloclist[i] = NULL;
		}

		gc.allocSize = gc.allocSize + STR_LEN_INC;
	}

	if((gc.alloclist[gc.length] =  malloc(size)) == NULL){
		printErr(ERR_INTERNAL);
		return NULL;
	}

	return gc.alloclist[gc.length++];
}

void *superRealloc(void* ptr, size_t size){
	for(int i = 0; i < gc.length; i++){
		if(gc.alloclist[i] == ptr){
			if((gc.alloclist[i] =  realloc(ptr, size)) == NULL){
				printErr(ERR_INTERNAL);
				return NULL;
			}

			return gc.alloclist[i];
		}
	}

	return superMalloc(size);
}

void superFree(void* ptr){
	for(int i = 0; i < gc.length; i++){
		if(gc.alloclist[i] == ptr){
			gc.alloclist[i] = NULL;
			break;
		}
	}

	free(ptr);
}

void gcClear(){
	for(int i = 0; i < gc.length; i++){
		free(gc.alloclist[i]);
	}

	free(gc.alloclist);
}

void gcPrint(){
	for(int i = 0; i < gc.allocSize; i++){
		printf("%p\n",gc.alloclist[i]);
	}
}