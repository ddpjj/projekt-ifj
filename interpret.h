/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

enum Instructions {
	/* Arithmetic Instructions */
/*OP: 0*/	I_ADD, // &2 + &3 => &1
/*OP: 1*/	I_SUB, // &2 - &3 => &1
/*OP: 2*/	I_MUL, // &2 * &3 => &1
/*OP: 3*/	I_DIV, // &2 / &3 => &1
/*OP: 4*/	I_MOV, // &2 => &1

	/* Comparison */
/*OP: 5*/	I_ISEQUAL, // (&2 == &3)?1:0 => &1
/*OP: 6*/	I_ISNOTEQUAL, // (&2 != &3)?1:0 => &1
/*OP: 7*/	I_ISABOVE, // (&2 > &3)?1:0 => &1
/*OP: 8*/	I_ISBELOW, // (&2 < &3)?1:0 => &1
/*OP: 9*/	I_ISEQABOVE, // (&2 >= &3)?1:0 => &1
/*OP: 10*/	I_ISEQBELOW, // (&2 <= &3)?1:0 => &1

	/* Input/Output */
/*OP: 11*/	I_READ, // STDIN => &1
/*OP: 12*/	I_WRITE, // &1 => STDOUT

	/* Jumps */
/*OP: 13*/	I_JMP, // Jump to &1
/*OP: 14*/	I_JMPE, // if (&2 == TRUE) then jump to &1
/*OP: 15*/	I_LABEL, // Create Label to jump to

	/* Conversion */
/*OP: 16*/	I_INT_TO_REAL,

	/* Functions */
/*OP: 17*/	I_MAKE_FRAME, // prvni adresa je adresa lokalni tabulky symbolu dane fce
/*OP: 18*/	I_SET_PAR, // z aktivniho ramce vezme parametr fce (addr2) a zkopiruje do pripraveneho ramce fce (addr1)
/*OP: 19*/	I_CALL, // instrukce na prevedeni cekajiciho ramce na aktivni, ulozi si do zasobniku klic pro navratovy skok a klic pro navratovou promennou
/*OP: 20*/	I_RETURN, // ukonci ramec, vrati navratovou hodnotu, smaze ukonceny ramec a provede navratovy skok

};


int interpret(tList *L, tHtab * globalHtab);
