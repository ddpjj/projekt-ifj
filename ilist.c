/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "ilist.h"
#include "gc.h"

#define LIST_OK		0
#define LIST_ERROR	1
/* Interni konstanty pro stav seznamu*/


void listInit(tList *L)
/* Inicializace seznamu */
{
	L->First = NULL;
	L->Last = NULL;
	L->Act = NULL;
}

void listFree (tList *L)
/* Vytisteni prostoru po seznamu */
{
	if (L->First != NULL) {
		tListItem *Pom;
		do {
			Pom = L->First->ptrNext;
			free(L->First);
			L->First = Pom;
		} while (L->First != NULL);
		L->Act = NULL;
		L->Last = NULL;
	}
}

int listPush(tList *L, void* ptr)
/* Pridani prvku na konec seznamu */
{
	tListItem *NewInstr;
	if ((NewInstr = malloc(sizeof (tListItem))) == NULL) {
		return LIST_ERROR;
	}else {
		NewInstr->data = ptr;
		NewInstr->ptrNext = NULL;
		NewInstr->ptrPrev = NULL;

		if (L->First == NULL) {
			L->First = NewInstr;
		}else {
			L->Last->ptrNext = NewInstr;
			NewInstr->ptrPrev = L->Last;
		}
		L->Last=NewInstr;
	return LIST_OK;
	}
}

int listPushFirst(tList *L, void* ptr)
/* Pridani prvku na konec seznamu */
{
	tListItem *NewInstr;
	if ((NewInstr = malloc(sizeof (tListItem))) == NULL) {
		return LIST_ERROR;
	}else {
		NewInstr->data = ptr;
		NewInstr->ptrNext = NULL;
		NewInstr->ptrPrev = NULL;

		if (L->First == NULL) {
			L->Last = NewInstr;
		}else {
			L->First->ptrPrev = NewInstr;
			NewInstr->ptrNext = L->First;
		}
		L->First=NewInstr;
	return LIST_OK;
	}
}

int listPushAfterAct(tList *L, void* ptr)
/* Pridani prvku na konec seznamu */
{
	if (L->Act == NULL){
		return LIST_ERROR;
	}
	if (L->Act == L->Last) {
		return listPush(L, ptr);
	}

	tListItem *NewInstr;
	if ((NewInstr = malloc(sizeof (tListItem))) == NULL) {
		return LIST_ERROR;
	}else {
		NewInstr->data = ptr;
		NewInstr->ptrNext = L->Act->ptrNext;
		NewInstr->ptrPrev = L->Act;
		L->Act->ptrNext->ptrPrev = NewInstr;
		L->Act->ptrNext = NewInstr;

	return LIST_OK;
	}
}

int listPushBeforeAct(tList *L, void* ptr)
/* Pridani prvku pred aktivni */
{
	if (L->Act == NULL){
		return LIST_ERROR;
	}

	if (L->Act == L->First) {
		return listPushFirst(L, ptr);
	}

	tListItem *NewInstr;
	if ((NewInstr = malloc(sizeof (tListItem))) == NULL) {
		return LIST_ERROR;
	}else {
		NewInstr->data = ptr;
		NewInstr->ptrPrev = L->Act->ptrPrev;
		NewInstr->ptrNext = L->Act;
		L->Act->ptrPrev->ptrNext = NewInstr;
		L->Act->ptrPrev = NewInstr;

	return LIST_OK;
	}
}

void* listPop(tList *L)
/* Ziskani dat z posledniho prvku a jeho smazani */
{
	if(L->Last == NULL){
		return NULL;
	}	

	void* ptr;
	tListItem *oldelement;

	if ((oldelement = malloc(sizeof (tListItem))) == NULL) {
		return NULL;
	}

	ptr = L->Last->data;
	oldelement = L->Last;

	if (L->First == L->Last) {
		L->Last = NULL;
		L->First = NULL;
		L->Act = NULL;
	}else{
		L->Last = L->Last->ptrPrev;
		L->Last->ptrNext = NULL;
		if(L->Act == oldelement){
			L->Act = NULL;
		}
	}

	free(oldelement);
	return ptr;
}

void* listPopFirst(tList *L)
/* Ziskani dat z prvniho prvku a jeho smazani */
{
	if(L->First == NULL){
		return NULL;
	}	

	void* ptr;
	tListItem *oldelement;

	if ((oldelement = malloc(sizeof (tListItem))) == NULL) {
		return NULL;
	}

	ptr = L->First->data;
	oldelement = L->First;

	if (L->First == L->Last) {
		L->Last = NULL;
		L->First = NULL;
		L->Act = NULL;
	}else{
		L->First = L->First->ptrNext;
		L->First->ptrPrev = NULL;
		if(L->Act == oldelement){
			L->Act = NULL;
		}
	}

	free(oldelement);
	return ptr;
}

void* listPopAct(tList *L)
/* Ziskani dat z aktualniho prvku a jeho smazani */
{
	if(L->Act == NULL){
		return NULL;
	}	

	if (L->Act == L->First) {
		L->Act = NULL;
		return listPopFirst(L);
	}

	if (L->Act == L->Last) {
		L->Act = NULL;
		return listPop(L);
	}

	void* ptr;
	ptr = L->Act->data;

	L->Act->ptrPrev->ptrNext = L->Act->ptrNext;
	L->Act->ptrNext->ptrPrev = L->Act->ptrPrev;

	free(L->Act);
	L->Act = NULL;
	return ptr;
}

int listActive(tList *L){
	return (L->Act != NULL);
}

void listActFirst(tList *L)
/* Presunuti aktivity na prvni prvek */
{
	L->Act = L->First;
}

void listActLast(tList *L)
/* Presunuti aktivity na posledni prvek */
{
	L->Act = L->Last;
}

void listActNext(tList *L)
/* Presunuti aktivity na nasledujici prvek */
{
	if (L->Act != NULL) {
		L->Act = L->Act->ptrNext;
	}
}

void listActPrev(tList *L)
/* Presunuti aktivity na predchozi prvek */
{
	if (L->Act != NULL) {
		L->Act = L->Act->ptrPrev;
	}
}

void listActGoto(tList *L, void *instr)
/* Presunuti aktivity na specifikovany prvek */
{	
	L->Act = L->First;
	while(L->Act != NULL){
		if(L->Act->data == instr){
			break;
		}
		L->Act = L->Act->ptrNext;
	}
}

void *listGetPtrFirst(tList *L)
/* Ziskani pointeru na prvni prvek */
{
	return (void*) L->First;
}

void *listGetPtrLast(tList *L)
/* Ziskani pointeru na posledni prvek */
{
	return (void*) L->Last;
}

void *listGetPtrNext(tListItem* structure)
/* Ziskani pointeru na prvek za zadanym */
{
	return (void*) structure->ptrNext;
}

void *listGetPtrPrev(tListItem* structure)
/* Ziskani pointeru na prvek pred zadanym */
{
	return (void*) structure->ptrPrev;
}

void* listGetAct(tList *L)
/* Ziskani dat z aktivniho prvku */
{
	if (L->Act == NULL) {
		return NULL;
	}
	return L->Act->data;
}

void* listGetFirst(tList *L)
/* Ziskani dat z prvniho prvku */
{
	if (L->First == NULL) {
		return NULL;
	}
	return L->First->data;
}

void* listGetLast(tList *L)
/* Ziskani dat z posledniho prvku */
{
	if (L->Last == NULL) {
		return NULL;
	}
	return L->Last->data;
}


/* funkce která vypíše celý seznam pokud je seznam naplněn stringy (pomocná funkce pro testy) */
void listCharPrint(tList *L){

	int position = 0;
	listActFirst(L);
	
	while (L->Act != NULL){
		printf("%i: %s \n", position, (char*) L->Act->data);
		listActNext(L);
		position++;
	}
	printf("\n");
}