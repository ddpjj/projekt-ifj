/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

/* návratové kódy programu */
typedef enum ecodes {
	OK = 0,
	ERR_LEXICAL, // 1 - Chyba v programu v rámci lexikální analýzy (chybná struktura aktuálního lexému)
	ERR_SYNTAX, // 2 - Chyba v programu v rámci syntaktické analýzy (chyba syntaxe struktury programu)
	ERR_SEM_DEF, // 3 - sémantický chyba v programu - nedefinovaná funkce/proměnná, pokus o redefinici funkce/proměnné, atd.
	ERR_SEM_TYPE, // 4 - sémantická chyba typové kompatibility v aritmetických, řetězcových a relačních výrazech, příp. špatný počet či typ parametrů u volání funkce.
	ERR_SEM_OTHER, // 5 - ostatní sémantické chyby.
	ERR_RUN_NUMBER, // 6 - běhová chyba při práci načítání číselné hodnoty ze vstupu
	ERR_RUN_UNINI_VAR, // 7 - běhová chyba při práci s neinicializovanou proměnnou
	ERR_RUN_ZERO_DIV, // 8 - běhová chyba dělení nulou.
	ERR_RUN_OTHER, // 9 - ostatní běhové chyby
	ERR_INTERNAL, // - interní chyba interpretu tj. neovlivněná vstupním programem (např. chyba alokace paměti, chyba při otevírání souboru s řídícím programem, špatné parametry příkazové řádky atd.
} Ecode;

int printErr(Ecode ecode); //vypise chybicku