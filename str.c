/**
 *	Implementace interpretu imperativního jazyka IFJ14
 *
 *	xbalvi00 David Balvín
 *	xbedna55 Drahoslav Bednář 
 *	xbenes38 Jakub Beneš
 *	xkunca07 Jiří Kunčák
 *	xlehne01 Pavla Lehnertová
 *
 */

/* knihovna pro práci s nekonečně dlouhými řetězci */
#include <string.h>
#include <malloc.h>
#include "str.h"
#include "gc.h"

#define STR_LEN_INC	8
/*konstanta ktera udava pocatecni velikost pro alokaci
kdyz bude retezec vetsi tak se budou pocitat nasobky teto konstanty*/

#define STR_OK		0
#define STR_ERROR	1
#define CI_MASK	32

int strCmpCI(char *s1, char *s2);
int strClear(string *s);

int strInit(string *s){
	/* inicialuzuje nový prázdný string */
	if((s->str = (char*) malloc(STR_LEN_INC)) == NULL){
		return STR_ERROR;
	}
	s->str[0] = '\0';
	s->length = 0;
	s->allocSize = STR_LEN_INC;
	return STR_OK;
}

void strFree(string *s){
	/* uvolní pamět po stringu */
	free(s->str);
	s->length = 0;
	s->allocSize = 0;
}

int strClear(string *s){
	/* nastaví řetězec na prázdný */
	if(s->allocSize != STR_LEN_INC){
		if((s->str = (char*) realloc(s->str, STR_LEN_INC)) == NULL){
			return STR_ERROR;
		}
		s->allocSize = STR_LEN_INC;
	}
	s->str[0] = '\0';
	s->length = 0;
	return STR_OK;
}

int strAddChar(string *s, char c){
	/* přidá na konec řetězce nový znak */
	if(s->length + 1  >= s->allocSize){
		if((s->str = (char*) realloc(s->str,(s->allocSize + STR_LEN_INC))) == NULL){
			return STR_ERROR;
		}
		s->allocSize = s->allocSize + STR_LEN_INC;
	}

	s->str[s->length] = c;
	s->length++;
	s->str[s->length] = '\0';
	return STR_OK;
}

int  strCopyString(string *s1, string *s2){
	/* zkopíruje string 2 do stringu 1 a změní velikost pokud jsou jiné */
	if(s1->allocSize != s2->allocSize){
		if((s1->str = (char*) realloc(s1->str, s2->allocSize)) == NULL){
			return STR_ERROR;
		}
	}

	s1->length = s2->length;
	s1->allocSize = s2->allocSize;
	strcpy(s1->str, s2->str);
	return STR_OK;
}

string * strClone(string *s){
	string * clon = (string*) malloc(sizeof(string));

	strInit(clon);
	strCopyString(clon,s);

	return clon;
}

string * strCreate(char *s){
	string * clon = (string*) malloc(sizeof(string));

	strInit(clon);
	strInsertStr(clon,s);

	return clon;
}

int strCmpString(string *s1, string *s2){
	/* porovná dva řetězce */
	if(s1 == s2){
		return STR_OK;
	}
	return(strCmpCI(s1->str, s2->str));
}

int strCmpConstStr(string *s1, char *s2){
	/* porovná string a konstantu */
	return(strCmpCI(s1->str, s2));
}

string * strConcStr(string *s1, string *s2){
/* Provede konkatenaci dvou stringů s2,s3 a uloží do s1 */

	string *concStr;
	concStr = strClone(s1);
	char *c;

	c = strGetStr(s2);
	while (*c != '\0'){
		strAddChar(concStr, *c);
		c++;
	}
	return concStr;
}

char *strGetStr(string *s){
	/* vratí řetězec */
	return s->str;
}

int strGetLength(string *s){
	/* vrací délku řetězce */
	return s->length;
}

int strCmpCI(char *s1, char *s2){
	int i = 0;
	char c,d;

	while(s1[i] != '\0' || s2[i] != '\0'){
		c = (s1[i]>='A' && s1[i]<='Z') ? (s1[i]|CI_MASK) : (s1[i]);
		d = (s2[i]>='A' && s2[i]<='Z') ? (s2[i]|CI_MASK) : (s2[i]);
		if(c != d){
			return STR_ERROR;
		}
		i++;
	}
	if(s1[i] == '\0' && s2[i] == '\0'){
		return STR_OK;
	}
	return STR_ERROR;
}

int  strInsertStr(string *s1, char *c){
	if(strClear(s1)){
		return STR_ERROR;
	}

	while (*c != '\0'){
		strAddChar(s1, *c);
		c++;
	}
	return STR_OK;
}

int toString(string *s1){
	char lastsymbol = 0;
	int comacount = 0;
	string helpstring;
	if(strInit(&helpstring) == STR_ERROR || strCopyString(&helpstring, s1) == STR_ERROR){
		return STR_ERROR;
	}
	char *string1 = strGetStr(&helpstring);
	if(strClear(s1) == STR_ERROR){
		return STR_ERROR;
	}
	for(; *string1 != '\0'; string1++){
		if(*string1 == '\''){
			if(lastsymbol == '\'' && (comacount % 2) == 0){
				if (strAddChar(s1, *string1) == STR_ERROR){
					return STR_ERROR;
				}
			}

			lastsymbol = *string1;
			comacount++;
			continue;
		}

		if(*string1 == '#' && lastsymbol == '\''){
			char number = 0;
			string1++;
			for(; *string1 != '\0'; string1++){
				if(*string1 == '\''){
					lastsymbol = *string1;
					comacount++;
					if (strAddChar(s1, number) == STR_ERROR){
						return STR_ERROR;
					}
					break;
				}

				number = number * 10 + (*string1 - '0');
				lastsymbol = *string1;
			}
			continue;
		}

		if (strAddChar(s1, *string1) == STR_ERROR){
			return STR_ERROR;
		}
		lastsymbol = *string1;
	}

	strClear(&helpstring);

	return 0;
}
